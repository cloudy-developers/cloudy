package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the inv_tipo_producto database table.
 * 
 */
@Entity
@Table(name="inv_tipo_producto")
@NamedQuery(name="InvTipoProducto.findAll", query="SELECT i FROM InvTipoProducto i")
public class InvTipoProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idtipoproduct;

	private String nomtipoproducto;

	//bi-directional many-to-one association to InvProducto
	@OneToMany(mappedBy="invTipoProducto")
	private List<InvProducto> invProductos;

	public InvTipoProducto() {
	}

	public Integer getIdtipoproduct() {
		return this.idtipoproduct;
	}

	public void setIdtipoproduct(Integer idtipoproduct) {
		this.idtipoproduct = idtipoproduct;
	}

	public String getNomtipoproducto() {
		return this.nomtipoproducto;
	}

	public void setNomtipoproducto(String nomtipoproducto) {
		this.nomtipoproducto = nomtipoproducto;
	}

	public List<InvProducto> getInvProductos() {
		return this.invProductos;
	}

	public void setInvProductos(List<InvProducto> invProductos) {
		this.invProductos = invProductos;
	}

	public InvProducto addInvProducto(InvProducto invProducto) {
		getInvProductos().add(invProducto);
		invProducto.setInvTipoProducto(this);

		return invProducto;
	}

	public InvProducto removeInvProducto(InvProducto invProducto) {
		getInvProductos().remove(invProducto);
		invProducto.setInvTipoProducto(null);

		return invProducto;
	}

}