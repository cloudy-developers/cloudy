package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the inv_stock database table.
 * 
 */
@Entity
@Table(name="inv_stock")
@NamedQuery(name="InvStock.findAll", query="SELECT i FROM InvStock i")
public class InvStock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idstock;

	@Column(name="cantidad_en_almacen")
	private Integer cantidadEnAlmacen;

	private String descripcion;

	private Integer maximo;

	private Integer minimo;

	//bi-directional many-to-one association to InvProducto
	@ManyToOne
	@JoinColumn(name="idproductops")
	private InvProducto invProducto;

	public InvStock() {
	}

	public Integer getIdstock() {
		return this.idstock;
	}

	public void setIdstock(Integer idstock) {
		this.idstock = idstock;
	}

	public Integer getCantidadEnAlmacen() {
		return this.cantidadEnAlmacen;
	}

	public void setCantidadEnAlmacen(Integer cantidadEnAlmacen) {
		this.cantidadEnAlmacen = cantidadEnAlmacen;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getMaximo() {
		return this.maximo;
	}

	public void setMaximo(Integer maximo) {
		this.maximo = maximo;
	}

	public Integer getMinimo() {
		return this.minimo;
	}

	public void setMinimo(Integer minimo) {
		this.minimo = minimo;
	}

	public InvProducto getInvProducto() {
		return this.invProducto;
	}

	public void setInvProducto(InvProducto invProducto) {
		this.invProducto = invProducto;
	}

}