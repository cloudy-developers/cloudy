package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the inv_tipo_caracteristica database table.
 * 
 */
@Entity
@Table(name="inv_tipo_caracteristica")
@NamedQuery(name="InvTipoCaracteristica.findAll", query="SELECT i FROM InvTipoCaracteristica i")
public class InvTipoCaracteristica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idtipo_carac")
	private Integer idtipoCarac;

	@Column(name="nombre_tipo")
	private String nombreTipo;

	//bi-directional many-to-one association to InvProductoCaracteristica
	@OneToMany(mappedBy="invTipoCaracteristica")
	private List<InvProductoCaracteristica> invProductoCaracteristicas;

	public InvTipoCaracteristica() {
	}

	public Integer getIdtipoCarac() {
		return this.idtipoCarac;
	}

	public void setIdtipoCarac(Integer idtipoCarac) {
		this.idtipoCarac = idtipoCarac;
	}

	public String getNombreTipo() {
		return this.nombreTipo;
	}

	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}

	public List<InvProductoCaracteristica> getInvProductoCaracteristicas() {
		return this.invProductoCaracteristicas;
	}

	public void setInvProductoCaracteristicas(List<InvProductoCaracteristica> invProductoCaracteristicas) {
		this.invProductoCaracteristicas = invProductoCaracteristicas;
	}

	public InvProductoCaracteristica addInvProductoCaracteristica(InvProductoCaracteristica invProductoCaracteristica) {
		getInvProductoCaracteristicas().add(invProductoCaracteristica);
		invProductoCaracteristica.setInvTipoCaracteristica(this);

		return invProductoCaracteristica;
	}

	public InvProductoCaracteristica removeInvProductoCaracteristica(InvProductoCaracteristica invProductoCaracteristica) {
		getInvProductoCaracteristicas().remove(invProductoCaracteristica);
		invProductoCaracteristica.setInvTipoCaracteristica(null);

		return invProductoCaracteristica;
	}

}