package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the inv_valor_estandar database table.
 * 
 */
@Entity
@Table(name="inv_valor_estandar")
@NamedQuery(name="InvValorEstandar.findAll", query="SELECT i FROM InvValorEstandar i")
public class InvValorEstandar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idvalor_estand")
	private Integer idvalorEstand;

	@Column(name="valor_estandar")
	private BigDecimal valorEstandar;

	//bi-directional many-to-one association to InvProducto
	@ManyToOne
	@JoinColumn(name="idproductopv")
	private InvProducto invProducto;

	public InvValorEstandar() {
	}

	public Integer getIdvalorEstand() {
		return this.idvalorEstand;
	}

	public void setIdvalorEstand(Integer idvalorEstand) {
		this.idvalorEstand = idvalorEstand;
	}

	public BigDecimal getValorEstandar() {
		return this.valorEstandar;
	}

	public void setValorEstandar(BigDecimal valorEstandar) {
		this.valorEstandar = valorEstandar;
	}

	public InvProducto getInvProducto() {
		return this.invProducto;
	}

	public void setInvProducto(InvProducto invProducto) {
		this.invProducto = invProducto;
	}

}