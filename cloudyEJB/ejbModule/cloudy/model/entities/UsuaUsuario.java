package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the usua_usuario database table.
 * 
 */
@Entity
@Table(name="usua_usuario")
@NamedQuery(name="UsuaUsuario.findAll", query="SELECT u FROM UsuaUsuario u")
public class UsuaUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idusuario;

	private String apellido;

	private String contrasena;

	private String email;

	private String nombre;

	private String telefono;

	//bi-directional many-to-one association to Dpa
	@ManyToOne
	@JoinColumn(name="iddivpoliticapu")
	private Dpa dpa;

	//bi-directional many-to-one association to UsuaRole
	@ManyToOne
	@JoinColumn(name="idrolp")
	private UsuaRole usuaRole;

	//bi-directional many-to-one association to VenRecibo
	@OneToMany(mappedBy="usuaUsuario")
	private List<VenRecibo> venRecibos;

	public UsuaUsuario() {
	}

	public Integer getIdusuario() {
		return this.idusuario;
	}

	public void setIdusuario(Integer idusuario) {
		this.idusuario = idusuario;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getContrasena() {
		return this.contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Dpa getDpa() {
		return this.dpa;
	}

	public void setDpa(Dpa dpa) {
		this.dpa = dpa;
	}

	public UsuaRole getUsuaRole() {
		return this.usuaRole;
	}

	public void setUsuaRole(UsuaRole usuaRole) {
		this.usuaRole = usuaRole;
	}

	public List<VenRecibo> getVenRecibos() {
		return this.venRecibos;
	}

	public void setVenRecibos(List<VenRecibo> venRecibos) {
		this.venRecibos = venRecibos;
	}

	public VenRecibo addVenRecibo(VenRecibo venRecibo) {
		getVenRecibos().add(venRecibo);
		venRecibo.setUsuaUsuario(this);

		return venRecibo;
	}

	public VenRecibo removeVenRecibo(VenRecibo venRecibo) {
		getVenRecibos().remove(venRecibo);
		venRecibo.setUsuaUsuario(null);

		return venRecibo;
	}

}