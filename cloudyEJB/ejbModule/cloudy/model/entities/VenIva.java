package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the ven_iva database table.
 * 
 */
@Entity
@Table(name="ven_iva")
@NamedQuery(name="VenIva.findAll", query="SELECT v FROM VenIva v")
public class VenIva implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idiva;

	private BigDecimal iva;

	//bi-directional many-to-one association to InvProducto
	@OneToMany(mappedBy="venIva")
	private List<InvProducto> invProductos;

	//bi-directional many-to-one association to VenDetalle
	@OneToMany(mappedBy="venIva")
	private List<VenDetalle> venDetalles;

	public VenIva() {
	}

	public Integer getIdiva() {
		return this.idiva;
	}

	public void setIdiva(Integer idiva) {
		this.idiva = idiva;
	}

	public BigDecimal getIva() {
		return this.iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public List<InvProducto> getInvProductos() {
		return this.invProductos;
	}

	public void setInvProductos(List<InvProducto> invProductos) {
		this.invProductos = invProductos;
	}

	public InvProducto addInvProducto(InvProducto invProducto) {
		getInvProductos().add(invProducto);
		invProducto.setVenIva(this);

		return invProducto;
	}

	public InvProducto removeInvProducto(InvProducto invProducto) {
		getInvProductos().remove(invProducto);
		invProducto.setVenIva(null);

		return invProducto;
	}

	public List<VenDetalle> getVenDetalles() {
		return this.venDetalles;
	}

	public void setVenDetalles(List<VenDetalle> venDetalles) {
		this.venDetalles = venDetalles;
	}

	public VenDetalle addVenDetalle(VenDetalle venDetalle) {
		getVenDetalles().add(venDetalle);
		venDetalle.setVenIva(this);

		return venDetalle;
	}

	public VenDetalle removeVenDetalle(VenDetalle venDetalle) {
		getVenDetalles().remove(venDetalle);
		venDetalle.setVenIva(null);

		return venDetalle;
	}

}