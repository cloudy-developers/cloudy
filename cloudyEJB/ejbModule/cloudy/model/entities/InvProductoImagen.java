package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the inv_producto_imagen database table.
 * 
 */
@Entity
@Table(name="inv_producto_imagen")
@NamedQuery(name="InvProductoImagen.findAll", query="SELECT i FROM InvProductoImagen i")
public class InvProductoImagen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idimagen;

	@Column(name="direccion_imagen")
	private String direccionImagen;

	//bi-directional many-to-one association to InvProducto
	@ManyToOne
	@JoinColumn(name="idproductopi")
	private InvProducto invProducto;

	public InvProductoImagen() {
	}

	public Integer getIdimagen() {
		return this.idimagen;
	}

	public void setIdimagen(Integer idimagen) {
		this.idimagen = idimagen;
	}

	public String getDireccionImagen() {
		return this.direccionImagen;
	}

	public void setDireccionImagen(String direccionImagen) {
		this.direccionImagen = direccionImagen;
	}

	public InvProducto getInvProducto() {
		return this.invProducto;
	}

	public void setInvProducto(InvProducto invProducto) {
		this.invProducto = invProducto;
	}

}