package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ven_recibo database table.
 * 
 */
@Entity
@Table(name="ven_recibo")
@NamedQuery(name="VenRecibo.findAll", query="SELECT v FROM VenRecibo v")
public class VenRecibo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idrecibo;

	@Column(name="base_cero")
	private BigDecimal baseCero;

	@Column(name="base_imponible")
	private BigDecimal baseImponible;

	@Column(name="cedula_cliente")
	private String cedulaCliente;

	@Column(name="codigo_recibo")
	private String codigoRecibo;

	@Column(name="correo_cliente")
	private String correoCliente;

	private BigDecimal descuento;

	@Column(name="direccion_cliente")
	private String direccionCliente;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_recibo")
	private Date fechaRecibo;

	@Column(name="nombre_cliente")
	private String nombreCliente;

	private BigDecimal subtotal;

	@Column(name="telefono_cliente")
	private String telefonoCliente;

	@Column(name="total_pago")
	private BigDecimal totalPago;

	//bi-directional many-to-one association to VenDetalle
	@OneToMany(mappedBy="venRecibo")
	private List<VenDetalle> venDetalles;

	//bi-directional many-to-one association to Empresa
	@ManyToOne
	@JoinColumn(name="idempresap")
	private Empresa empresa;

	//bi-directional many-to-one association to UsuaUsuario
	@ManyToOne
	@JoinColumn(name="idusuariop")
	private UsuaUsuario usuaUsuario;

	//bi-directional many-to-one association to VenFormaPago
	@ManyToOne
	@JoinColumn(name="id_forma_pago")
	private VenFormaPago venFormaPago;

	public VenRecibo() {
	}

	public Integer getIdrecibo() {
		return this.idrecibo;
	}

	public void setIdrecibo(Integer idrecibo) {
		this.idrecibo = idrecibo;
	}

	public BigDecimal getBaseCero() {
		return this.baseCero;
	}

	public void setBaseCero(BigDecimal baseCero) {
		this.baseCero = baseCero;
	}

	public BigDecimal getBaseImponible() {
		return this.baseImponible;
	}

	public void setBaseImponible(BigDecimal baseImponible) {
		this.baseImponible = baseImponible;
	}

	public String getCedulaCliente() {
		return this.cedulaCliente;
	}

	public void setCedulaCliente(String cedulaCliente) {
		this.cedulaCliente = cedulaCliente;
	}

	public String getCodigoRecibo() {
		return this.codigoRecibo;
	}

	public void setCodigoRecibo(String codigoRecibo) {
		this.codigoRecibo = codigoRecibo;
	}

	public String getCorreoCliente() {
		return this.correoCliente;
	}

	public void setCorreoCliente(String correoCliente) {
		this.correoCliente = correoCliente;
	}

	public BigDecimal getDescuento() {
		return this.descuento;
	}

	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}

	public String getDireccionCliente() {
		return this.direccionCliente;
	}

	public void setDireccionCliente(String direccionCliente) {
		this.direccionCliente = direccionCliente;
	}

	public Date getFechaRecibo() {
		return this.fechaRecibo;
	}

	public void setFechaRecibo(Date fechaRecibo) {
		this.fechaRecibo = fechaRecibo;
	}

	public String getNombreCliente() {
		return this.nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public String getTelefonoCliente() {
		return this.telefonoCliente;
	}

	public void setTelefonoCliente(String telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}

	public BigDecimal getTotalPago() {
		return this.totalPago;
	}

	public void setTotalPago(BigDecimal totalPago) {
		this.totalPago = totalPago;
	}

	public List<VenDetalle> getVenDetalles() {
		return this.venDetalles;
	}

	public void setVenDetalles(List<VenDetalle> venDetalles) {
		this.venDetalles = venDetalles;
	}

	public VenDetalle addVenDetalle(VenDetalle venDetalle) {
		getVenDetalles().add(venDetalle);
		venDetalle.setVenRecibo(this);

		return venDetalle;
	}

	public VenDetalle removeVenDetalle(VenDetalle venDetalle) {
		getVenDetalles().remove(venDetalle);
		venDetalle.setVenRecibo(null);

		return venDetalle;
	}

	public Empresa getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public UsuaUsuario getUsuaUsuario() {
		return this.usuaUsuario;
	}

	public void setUsuaUsuario(UsuaUsuario usuaUsuario) {
		this.usuaUsuario = usuaUsuario;
	}

	public VenFormaPago getVenFormaPago() {
		return this.venFormaPago;
	}

	public void setVenFormaPago(VenFormaPago venFormaPago) {
		this.venFormaPago = venFormaPago;
	}

}