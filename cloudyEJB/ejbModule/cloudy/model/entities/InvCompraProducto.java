package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the inv_compra_producto database table.
 * 
 */
@Entity
@Table(name="inv_compra_producto")
@NamedQuery(name="InvCompraProducto.findAll", query="SELECT i FROM InvCompraProducto i")
public class InvCompraProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idcompra;

	@Column(name="cantidad_compra")
	private BigDecimal cantidadCompra;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_compra")
	private Date fechaCompra;

	@Column(name="valor_compra")
	private BigDecimal valorCompra;

	//bi-directional many-to-one association to InvProducto
	@ManyToOne
	@JoinColumn(name="idproductopc")
	private InvProducto invProducto;

	public InvCompraProducto() {
	}

	public Integer getIdcompra() {
		return this.idcompra;
	}

	public void setIdcompra(Integer idcompra) {
		this.idcompra = idcompra;
	}

	public BigDecimal getCantidadCompra() {
		return this.cantidadCompra;
	}

	public void setCantidadCompra(BigDecimal cantidadCompra) {
		this.cantidadCompra = cantidadCompra;
	}

	public Date getFechaCompra() {
		return this.fechaCompra;
	}

	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public BigDecimal getValorCompra() {
		return this.valorCompra;
	}

	public void setValorCompra(BigDecimal valorCompra) {
		this.valorCompra = valorCompra;
	}

	public InvProducto getInvProducto() {
		return this.invProducto;
	}

	public void setInvProducto(InvProducto invProducto) {
		this.invProducto = invProducto;
	}

}