package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the inv_producto_caracteristica database table.
 * 
 */
@Entity
@Table(name="inv_producto_caracteristica")
@NamedQuery(name="InvProductoCaracteristica.findAll", query="SELECT i FROM InvProductoCaracteristica i")
public class InvProductoCaracteristica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idcaracteristica;

	@Column(name="descripcion_caracteristica")
	private String descripcionCaracteristica;

	//bi-directional many-to-one association to InvProducto
	@ManyToOne
	@JoinColumn(name="idproductop")
	private InvProducto invProducto;

	//bi-directional many-to-one association to InvTipoCaracteristica
	@ManyToOne
	@JoinColumn(name="idtipo_caracp")
	private InvTipoCaracteristica invTipoCaracteristica;

	public InvProductoCaracteristica() {
	}

	public Integer getIdcaracteristica() {
		return this.idcaracteristica;
	}

	public void setIdcaracteristica(Integer idcaracteristica) {
		this.idcaracteristica = idcaracteristica;
	}

	public String getDescripcionCaracteristica() {
		return this.descripcionCaracteristica;
	}

	public void setDescripcionCaracteristica(String descripcionCaracteristica) {
		this.descripcionCaracteristica = descripcionCaracteristica;
	}

	public InvProducto getInvProducto() {
		return this.invProducto;
	}

	public void setInvProducto(InvProducto invProducto) {
		this.invProducto = invProducto;
	}

	public InvTipoCaracteristica getInvTipoCaracteristica() {
		return this.invTipoCaracteristica;
	}

	public void setInvTipoCaracteristica(InvTipoCaracteristica invTipoCaracteristica) {
		this.invTipoCaracteristica = invTipoCaracteristica;
	}

}