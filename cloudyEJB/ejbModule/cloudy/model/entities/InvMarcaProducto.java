package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the inv_marca_producto database table.
 * 
 */
@Entity
@Table(name="inv_marca_producto")
@NamedQuery(name="InvMarcaProducto.findAll", query="SELECT i FROM InvMarcaProducto i")
public class InvMarcaProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idmarca;

	@Column(name="nombre_marca")
	private String nombreMarca;

	//bi-directional many-to-one association to InvProducto
	@OneToMany(mappedBy="invMarcaProducto")
	private List<InvProducto> invProductos;

	public InvMarcaProducto() {
	}

	public Integer getIdmarca() {
		return this.idmarca;
	}

	public void setIdmarca(Integer idmarca) {
		this.idmarca = idmarca;
	}

	public String getNombreMarca() {
		return this.nombreMarca;
	}

	public void setNombreMarca(String nombreMarca) {
		this.nombreMarca = nombreMarca;
	}

	public List<InvProducto> getInvProductos() {
		return this.invProductos;
	}

	public void setInvProductos(List<InvProducto> invProductos) {
		this.invProductos = invProductos;
	}

	public InvProducto addInvProducto(InvProducto invProducto) {
		getInvProductos().add(invProducto);
		invProducto.setInvMarcaProducto(this);

		return invProducto;
	}

	public InvProducto removeInvProducto(InvProducto invProducto) {
		getInvProductos().remove(invProducto);
		invProducto.setInvMarcaProducto(null);

		return invProducto;
	}

}