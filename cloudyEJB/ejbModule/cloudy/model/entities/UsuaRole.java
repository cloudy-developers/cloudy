package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the usua_roles database table.
 * 
 */
@Entity
@Table(name="usua_roles")
@NamedQuery(name="UsuaRole.findAll", query="SELECT u FROM UsuaRole u")
public class UsuaRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idrol;

	private String nomrol;

	//bi-directional many-to-one association to UsuaUsuario
	@OneToMany(mappedBy="usuaRole")
	private List<UsuaUsuario> usuaUsuarios;

	public UsuaRole() {
	}

	public Integer getIdrol() {
		return this.idrol;
	}

	public void setIdrol(Integer idrol) {
		this.idrol = idrol;
	}

	public String getNomrol() {
		return this.nomrol;
	}

	public void setNomrol(String nomrol) {
		this.nomrol = nomrol;
	}

	public List<UsuaUsuario> getUsuaUsuarios() {
		return this.usuaUsuarios;
	}

	public void setUsuaUsuarios(List<UsuaUsuario> usuaUsuarios) {
		this.usuaUsuarios = usuaUsuarios;
	}

	public UsuaUsuario addUsuaUsuario(UsuaUsuario usuaUsuario) {
		getUsuaUsuarios().add(usuaUsuario);
		usuaUsuario.setUsuaRole(this);

		return usuaUsuario;
	}

	public UsuaUsuario removeUsuaUsuario(UsuaUsuario usuaUsuario) {
		getUsuaUsuarios().remove(usuaUsuario);
		usuaUsuario.setUsuaRole(null);

		return usuaUsuario;
	}

}