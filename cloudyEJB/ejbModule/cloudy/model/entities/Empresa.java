package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the empresa database table.
 * 
 */
@Entity
@NamedQuery(name="Empresa.findAll", query="SELECT e FROM Empresa e")
public class Empresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idempresa;

	private String canton;

	private String correo;

	private String direccion;

	private String nombre;

	//bi-directional many-to-one association to EmpresaContacto
	@OneToMany(mappedBy="empresa")
	private List<EmpresaContacto> empresaContactos;

	//bi-directional many-to-one association to VenRecibo
	@OneToMany(mappedBy="empresa")
	private List<VenRecibo> venRecibos;

	public Empresa() {
	}

	public Integer getIdempresa() {
		return this.idempresa;
	}

	public void setIdempresa(Integer idempresa) {
		this.idempresa = idempresa;
	}

	public String getCanton() {
		return this.canton;
	}

	public void setCanton(String canton) {
		this.canton = canton;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<EmpresaContacto> getEmpresaContactos() {
		return this.empresaContactos;
	}

	public void setEmpresaContactos(List<EmpresaContacto> empresaContactos) {
		this.empresaContactos = empresaContactos;
	}

	public EmpresaContacto addEmpresaContacto(EmpresaContacto empresaContacto) {
		getEmpresaContactos().add(empresaContacto);
		empresaContacto.setEmpresa(this);

		return empresaContacto;
	}

	public EmpresaContacto removeEmpresaContacto(EmpresaContacto empresaContacto) {
		getEmpresaContactos().remove(empresaContacto);
		empresaContacto.setEmpresa(null);

		return empresaContacto;
	}

	public List<VenRecibo> getVenRecibos() {
		return this.venRecibos;
	}

	public void setVenRecibos(List<VenRecibo> venRecibos) {
		this.venRecibos = venRecibos;
	}

	public VenRecibo addVenRecibo(VenRecibo venRecibo) {
		getVenRecibos().add(venRecibo);
		venRecibo.setEmpresa(this);

		return venRecibo;
	}

	public VenRecibo removeVenRecibo(VenRecibo venRecibo) {
		getVenRecibos().remove(venRecibo);
		venRecibo.setEmpresa(null);

		return venRecibo;
	}

}