package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ven_detalle database table.
 * 
 */
@Entity
@Table(name="ven_detalle")
@NamedQuery(name="VenDetalle.findAll", query="SELECT v FROM VenDetalle v")
public class VenDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer iddetalle;

	private BigDecimal cantidad;

	@Column(name="valor_producto")
	private BigDecimal valorProducto;

	//bi-directional many-to-one association to InvProducto
	@ManyToOne
	@JoinColumn(name="idproductopd")
	private InvProducto invProducto;

	//bi-directional many-to-one association to VenIva
	@ManyToOne
	@JoinColumn(name="idivap")
	private VenIva venIva;

	//bi-directional many-to-one association to VenRecibo
	@ManyToOne
	@JoinColumn(name="idrecibop")
	private VenRecibo venRecibo;

	public VenDetalle() {
	}

	public Integer getIddetalle() {
		return this.iddetalle;
	}

	public void setIddetalle(Integer iddetalle) {
		this.iddetalle = iddetalle;
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getValorProducto() {
		return this.valorProducto;
	}

	public void setValorProducto(BigDecimal valorProducto) {
		this.valorProducto = valorProducto;
	}

	public InvProducto getInvProducto() {
		return this.invProducto;
	}

	public void setInvProducto(InvProducto invProducto) {
		this.invProducto = invProducto;
	}

	public VenIva getVenIva() {
		return this.venIva;
	}

	public void setVenIva(VenIva venIva) {
		this.venIva = venIva;
	}

	public VenRecibo getVenRecibo() {
		return this.venRecibo;
	}

	public void setVenRecibo(VenRecibo venRecibo) {
		this.venRecibo = venRecibo;
	}

}