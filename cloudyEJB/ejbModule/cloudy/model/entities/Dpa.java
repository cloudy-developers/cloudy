package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the dpa database table.
 * 
 */
@Entity
@NamedQuery(name="Dpa.findAll", query="SELECT d FROM Dpa d")
public class Dpa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String iddivpolitica;

	private BigDecimal nivel;

	private String nomdivpolitica;

	private String ultimonivel;

	//bi-directional many-to-one association to Dpa
	@ManyToOne
	@JoinColumn(name="iddivpoliticap")
	private Dpa dpa;

	//bi-directional many-to-one association to Dpa
	@OneToMany(mappedBy="dpa")
	private List<Dpa> dpas;

	//bi-directional many-to-one association to UsuaUsuario
	@OneToMany(mappedBy="dpa")
	private List<UsuaUsuario> usuaUsuarios;

	public Dpa() {
	}

	public String getIddivpolitica() {
		return this.iddivpolitica;
	}

	public void setIddivpolitica(String iddivpolitica) {
		this.iddivpolitica = iddivpolitica;
	}

	public BigDecimal getNivel() {
		return this.nivel;
	}

	public void setNivel(BigDecimal nivel) {
		this.nivel = nivel;
	}

	public String getNomdivpolitica() {
		return this.nomdivpolitica;
	}

	public void setNomdivpolitica(String nomdivpolitica) {
		this.nomdivpolitica = nomdivpolitica;
	}

	public String getUltimonivel() {
		return this.ultimonivel;
	}

	public void setUltimonivel(String ultimonivel) {
		this.ultimonivel = ultimonivel;
	}

	public Dpa getDpa() {
		return this.dpa;
	}

	public void setDpa(Dpa dpa) {
		this.dpa = dpa;
	}

	public List<Dpa> getDpas() {
		return this.dpas;
	}

	public void setDpas(List<Dpa> dpas) {
		this.dpas = dpas;
	}

	public Dpa addDpa(Dpa dpa) {
		getDpas().add(dpa);
		dpa.setDpa(this);

		return dpa;
	}

	public Dpa removeDpa(Dpa dpa) {
		getDpas().remove(dpa);
		dpa.setDpa(null);

		return dpa;
	}

	public List<UsuaUsuario> getUsuaUsuarios() {
		return this.usuaUsuarios;
	}

	public void setUsuaUsuarios(List<UsuaUsuario> usuaUsuarios) {
		this.usuaUsuarios = usuaUsuarios;
	}

	public UsuaUsuario addUsuaUsuario(UsuaUsuario usuaUsuario) {
		getUsuaUsuarios().add(usuaUsuario);
		usuaUsuario.setDpa(this);

		return usuaUsuario;
	}

	public UsuaUsuario removeUsuaUsuario(UsuaUsuario usuaUsuario) {
		getUsuaUsuarios().remove(usuaUsuario);
		usuaUsuario.setDpa(null);

		return usuaUsuario;
	}

}