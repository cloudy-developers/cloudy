package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ven_forma_pago database table.
 * 
 */
@Entity
@Table(name="ven_forma_pago")
@NamedQuery(name="VenFormaPago.findAll", query="SELECT v FROM VenFormaPago v")
public class VenFormaPago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idforma_pago")
	private Integer idformaPago;

	@Column(name="nombre_forma_pago")
	private String nombreFormaPago;

	//bi-directional many-to-one association to VenRecibo
	@OneToMany(mappedBy="venFormaPago")
	private List<VenRecibo> venRecibos;

	public VenFormaPago() {
	}

	public Integer getIdformaPago() {
		return this.idformaPago;
	}

	public void setIdformaPago(Integer idformaPago) {
		this.idformaPago = idformaPago;
	}

	public String getNombreFormaPago() {
		return this.nombreFormaPago;
	}

	public void setNombreFormaPago(String nombreFormaPago) {
		this.nombreFormaPago = nombreFormaPago;
	}

	public List<VenRecibo> getVenRecibos() {
		return this.venRecibos;
	}

	public void setVenRecibos(List<VenRecibo> venRecibos) {
		this.venRecibos = venRecibos;
	}

	public VenRecibo addVenRecibo(VenRecibo venRecibo) {
		getVenRecibos().add(venRecibo);
		venRecibo.setVenFormaPago(this);

		return venRecibo;
	}

	public VenRecibo removeVenRecibo(VenRecibo venRecibo) {
		getVenRecibos().remove(venRecibo);
		venRecibo.setVenFormaPago(null);

		return venRecibo;
	}

}