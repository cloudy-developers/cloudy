package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the empresa_contacto database table.
 * 
 */
@Entity
@Table(name="empresa_contacto")
@NamedQuery(name="EmpresaContacto.findAll", query="SELECT e FROM EmpresaContacto e")
public class EmpresaContacto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idcontacto;

	@Column(name="numero_contacto")
	private String numeroContacto;

	//bi-directional many-to-one association to Empresa
	@ManyToOne
	@JoinColumn(name="idempresap")
	private Empresa empresa;

	public EmpresaContacto() {
	}

	public Integer getIdcontacto() {
		return this.idcontacto;
	}

	public void setIdcontacto(Integer idcontacto) {
		this.idcontacto = idcontacto;
	}

	public String getNumeroContacto() {
		return this.numeroContacto;
	}

	public void setNumeroContacto(String numeroContacto) {
		this.numeroContacto = numeroContacto;
	}

	public Empresa getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

}