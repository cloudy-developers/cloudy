package cloudy.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the inv_producto database table.
 * 
 */
@Entity
@Table(name="inv_producto")
@NamedQuery(name="InvProducto.findAll", query="SELECT i FROM InvProducto i")
public class InvProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idproducto;

	@Column(name="codigo_producto")
	private String codigoProducto;

	private String descripcion;

	@Column(name="nombre_producto")
	private String nombreProducto;

	//bi-directional many-to-one association to InvTipoProducto
	@ManyToOne
	@JoinColumn(name="idtipoproductopp")
	private InvTipoProducto invTipoProducto;

	//bi-directional many-to-one association to InvStock
	@OneToMany(mappedBy="invProducto")
	private List<InvStock> invStocks;

	//bi-directional many-to-one association to InvCompraProducto
	@OneToMany(mappedBy="invProducto")
	private List<InvCompraProducto> invCompraProductos;

	//bi-directional many-to-one association to InvMarcaProducto
	@ManyToOne
	@JoinColumn(name="idmarcap")
	private InvMarcaProducto invMarcaProducto;

	//bi-directional many-to-one association to VenIva
	@ManyToOne
	@JoinColumn(name="idivapp")
	private VenIva venIva;

	//bi-directional many-to-one association to InvProductoCaracteristica
	@OneToMany(mappedBy="invProducto")
	private List<InvProductoCaracteristica> invProductoCaracteristicas;

	//bi-directional many-to-one association to InvProductoImagen
	@OneToMany(mappedBy="invProducto")
	private List<InvProductoImagen> invProductoImagens;

	//bi-directional many-to-one association to InvValorEstandar
	@OneToMany(mappedBy="invProducto")
	private List<InvValorEstandar> invValorEstandars;

	//bi-directional many-to-one association to VenDetalle
	@OneToMany(mappedBy="invProducto")
	private List<VenDetalle> venDetalles;

	public InvProducto() {
	}

	public Integer getIdproducto() {
		return this.idproducto;
	}

	public void setIdproducto(Integer idproducto) {
		this.idproducto = idproducto;
	}

	public String getCodigoProducto() {
		return this.codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombreProducto() {
		return this.nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public InvTipoProducto getInvTipoProducto() {
		return this.invTipoProducto;
	}

	public void setInvTipoProducto(InvTipoProducto invTipoProducto) {
		this.invTipoProducto = invTipoProducto;
	}

	public List<InvStock> getInvStocks() {
		return this.invStocks;
	}

	public void setInvStocks(List<InvStock> invStocks) {
		this.invStocks = invStocks;
	}

	public InvStock addInvStock(InvStock invStock) {
		getInvStocks().add(invStock);
		invStock.setInvProducto(this);

		return invStock;
	}

	public InvStock removeInvStock(InvStock invStock) {
		getInvStocks().remove(invStock);
		invStock.setInvProducto(null);

		return invStock;
	}

	public List<InvCompraProducto> getInvCompraProductos() {
		return this.invCompraProductos;
	}

	public void setInvCompraProductos(List<InvCompraProducto> invCompraProductos) {
		this.invCompraProductos = invCompraProductos;
	}

	public InvCompraProducto addInvCompraProducto(InvCompraProducto invCompraProducto) {
		getInvCompraProductos().add(invCompraProducto);
		invCompraProducto.setInvProducto(this);

		return invCompraProducto;
	}

	public InvCompraProducto removeInvCompraProducto(InvCompraProducto invCompraProducto) {
		getInvCompraProductos().remove(invCompraProducto);
		invCompraProducto.setInvProducto(null);

		return invCompraProducto;
	}

	public InvMarcaProducto getInvMarcaProducto() {
		return this.invMarcaProducto;
	}

	public void setInvMarcaProducto(InvMarcaProducto invMarcaProducto) {
		this.invMarcaProducto = invMarcaProducto;
	}

	public VenIva getVenIva() {
		return this.venIva;
	}

	public void setVenIva(VenIva venIva) {
		this.venIva = venIva;
	}

	public List<InvProductoCaracteristica> getInvProductoCaracteristicas() {
		return this.invProductoCaracteristicas;
	}

	public void setInvProductoCaracteristicas(List<InvProductoCaracteristica> invProductoCaracteristicas) {
		this.invProductoCaracteristicas = invProductoCaracteristicas;
	}

	public InvProductoCaracteristica addInvProductoCaracteristica(InvProductoCaracteristica invProductoCaracteristica) {
		getInvProductoCaracteristicas().add(invProductoCaracteristica);
		invProductoCaracteristica.setInvProducto(this);

		return invProductoCaracteristica;
	}

	public InvProductoCaracteristica removeInvProductoCaracteristica(InvProductoCaracteristica invProductoCaracteristica) {
		getInvProductoCaracteristicas().remove(invProductoCaracteristica);
		invProductoCaracteristica.setInvProducto(null);

		return invProductoCaracteristica;
	}

	public List<InvProductoImagen> getInvProductoImagens() {
		return this.invProductoImagens;
	}

	public void setInvProductoImagens(List<InvProductoImagen> invProductoImagens) {
		this.invProductoImagens = invProductoImagens;
	}

	public InvProductoImagen addInvProductoImagen(InvProductoImagen invProductoImagen) {
		getInvProductoImagens().add(invProductoImagen);
		invProductoImagen.setInvProducto(this);

		return invProductoImagen;
	}

	public InvProductoImagen removeInvProductoImagen(InvProductoImagen invProductoImagen) {
		getInvProductoImagens().remove(invProductoImagen);
		invProductoImagen.setInvProducto(null);

		return invProductoImagen;
	}

	public List<InvValorEstandar> getInvValorEstandars() {
		return this.invValorEstandars;
	}

	public void setInvValorEstandars(List<InvValorEstandar> invValorEstandars) {
		this.invValorEstandars = invValorEstandars;
	}

	public InvValorEstandar addInvValorEstandar(InvValorEstandar invValorEstandar) {
		getInvValorEstandars().add(invValorEstandar);
		invValorEstandar.setInvProducto(this);

		return invValorEstandar;
	}

	public InvValorEstandar removeInvValorEstandar(InvValorEstandar invValorEstandar) {
		getInvValorEstandars().remove(invValorEstandar);
		invValorEstandar.setInvProducto(null);

		return invValorEstandar;
	}

	public List<VenDetalle> getVenDetalles() {
		return this.venDetalles;
	}

	public void setVenDetalles(List<VenDetalle> venDetalles) {
		this.venDetalles = venDetalles;
	}

	public VenDetalle addVenDetalle(VenDetalle venDetalle) {
		getVenDetalles().add(venDetalle);
		venDetalle.setInvProducto(this);

		return venDetalle;
	}

	public VenDetalle removeVenDetalle(VenDetalle venDetalle) {
		getVenDetalles().remove(venDetalle);
		venDetalle.setInvProducto(null);

		return venDetalle;
	}

}