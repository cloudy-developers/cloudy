package cloudy.model.manager;

import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.InvCompraProducto;
import cloudy.model.entities.InvProducto;

/**
 * Session Bean implementation class ManagerCompraProducto
 */
@Stateless
@LocalBean
public class ManagerCompraProducto 
{
	@PersistenceContext
	EntityManager  em;

    public ManagerCompraProducto() {}
    
    public List<InvCompraProducto> FindAllCompras()
    {
    	String consulta = "SELECT i FROM InvCompraProducto i";
    	Query q = em.createQuery(consulta, InvCompraProducto.class);
    	return q.getResultList();
    }
    
    public void IngresarCompra(InvCompraProducto compra, InvProducto producto)
    {
    	InvCompraProducto nuevacompra = new InvCompraProducto();
    	java.util.Date fecha = new Date();
    	
    	nuevacompra.setInvProducto(producto);
    	nuevacompra.setFechaCompra(fecha);
    	nuevacompra.setCantidadCompra(compra.getCantidadCompra());
    	nuevacompra.setValorCompra(compra.getValorCompra());
    	
    	em.persist(nuevacompra);
    }

}
