package cloudy.model.manager;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.Dpa;

/**
 * Session Bean implementation class ManagerDPA
 */
@Stateless
@LocalBean
public class ManagerDPA {

    @PersistenceContext
    EntityManager em;
   
    public ManagerDPA() {}
    
    public List<Dpa> findAllProvincias()
    {
    	String consulta = "SELECT d FROM Dpa d WHERE d.nivel = 1";
    	Query q  = em.createQuery(consulta, Dpa.class);
    	return q.getResultList();
    }
    
    public List<Dpa> findAllCantones()
    {
    	String consulta = "SELECT d FROM Dpa d WHERE d.nivel = 2";
    	Query q = em.createQuery(consulta, Dpa.class);
    	return q.getResultList();
    }
    
    public List<Dpa> findCantonesPorProvincia(List<Dpa> cantones, String idProvincia)
    {
    	List<Dpa> cantonespoProvincias = new ArrayList<>();
    	
    	for (int i = 0; i < cantones.size(); i++)
    	{
    		if (cantones.get(i).getIddivpolitica().substring(0, 2).equals(idProvincia))
    		{
				cantonespoProvincias.add(cantones.get(i));
			}
		}
    	
    	return cantonespoProvincias;
    }
    
    public Dpa findDPA(String iddpa)
    {
    	Dpa provincia = em.find(Dpa.class, iddpa);
    	return provincia;
    }

}
