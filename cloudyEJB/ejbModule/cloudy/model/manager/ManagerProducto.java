package cloudy.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.InvMarcaProducto;
import cloudy.model.entities.InvProducto;
import cloudy.model.entities.InvTipoProducto;
import cloudy.model.entities.VenIva;

/**
 * Session Bean implementation class ManagerProducto
 */
@Stateless
@LocalBean
public class ManagerProducto 
{
	@PersistenceContext
	EntityManager em;

    public ManagerProducto() {}
    
    public InvProducto IngresarProducto (VenIva iva, InvMarcaProducto marca, InvProducto producto, InvTipoProducto tipoproducto)
    {
    	InvProducto nuevoproducto = new InvProducto();
    	
    	nuevoproducto.setInvMarcaProducto(marca);
    	nuevoproducto.setVenIva(iva);
    	nuevoproducto.setInvTipoProducto(tipoproducto);
    	nuevoproducto.setCodigoProducto(producto.getCodigoProducto());
    	nuevoproducto.setDescripcion(producto.getDescripcion());
    	nuevoproducto.setNombreProducto(producto.getNombreProducto());
    	
    	em.persist(nuevoproducto);
    	
    	return this.findProductoByCodigoProducto(this.findAllProductos(), producto.getCodigoProducto());
    }
    
    public List<InvProducto> findAllProductos()
    {
    	String consulta = "SELECT i FROM InvProducto i";
    	Query q = em.createQuery(consulta, InvProducto.class);
    	return q.getResultList();
    }
    
    public List<InvProducto> findProductosNoStock()
    {
    	String consulta = "select a from InvProducto a left join InvStock b on a.idproducto = b.invProducto.idproducto where b.invProducto.idproducto is null";
    	Query q = em.createQuery(consulta, InvProducto.class);
    	return q.getResultList();
    }
    
    public InvProducto findProductoByCodigoProducto(List<InvProducto> productos, String codigo)
    {
    	InvProducto producto = new InvProducto();
    	for (int i = 0; i < productos.size(); i++)
    	{
			if(productos.get(i).getCodigoProducto().equals(codigo))
			{
				producto=productos.get(i);
				break;
			}
		}
    	return producto;
    }
    
    public InvProducto findByID(int idProducto)
    {
    	InvProducto producto = new InvProducto();
    	producto = em.find(InvProducto.class, idProducto);
    	return producto;
    }
    
    

}
