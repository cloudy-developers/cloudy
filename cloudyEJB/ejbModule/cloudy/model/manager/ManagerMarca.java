package cloudy.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.InvMarcaProducto;

/**
 * Session Bean implementation class ManagerMarca
 */
@Stateless
@LocalBean
public class ManagerMarca 
{
	@PersistenceContext
	EntityManager em;
    
    public ManagerMarca() {}
    
    public List<InvMarcaProducto> findAllMarcas()
    {
    	String consulta = "SELECT i FROM InvMarcaProducto i";
    	Query q = em.createQuery(consulta,InvMarcaProducto.class);
    	return q.getResultList();
    }
    
    public InvMarcaProducto findMarca(int idmarca)
    {
    	InvMarcaProducto marca = em.find(InvMarcaProducto.class, idmarca);
    	return marca;
    }
    
    public void IngresarNuevaMarca(InvMarcaProducto marca)
    {
    	InvMarcaProducto nuevamarca = new InvMarcaProducto();
    	nuevamarca.setNombreMarca(marca.getNombreMarca());
    	em.persist(nuevamarca);
    }
}
