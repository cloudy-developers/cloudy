package cloudy.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.VenFormaPago;

@Stateless
@LocalBean
public class ManagerFormaPago 
{
	@PersistenceContext
	EntityManager em;
	
    public ManagerFormaPago() {}
    
    public void IngresarFormaDePago(VenFormaPago formpago)
    {
    	VenFormaPago nuevaforma = new VenFormaPago();
    	nuevaforma.setNombreFormaPago(formpago.getNombreFormaPago());
    	em.persist(nuevaforma);
    }
    
    public VenFormaPago FindFormaPagoByNombre(String nombre)
    {
    	String consulta = "SELECT v FROM VenFormaPago v WHERE v.nombreFormaPago ='"+nombre+"'";
    	Query q = em.createQuery(consulta, VenFormaPago.class);
    	List<VenFormaPago> formasdepago = q.getResultList();
    	
    	return formasdepago.get(0);
    }
    
    public List<VenFormaPago> FindAllFormaPago()
    {
    	String consulta = "SELECT v FROM VenFormaPago v";
    	Query q = em.createQuery(consulta, VenFormaPago.class);
    	return q.getResultList();
    }

}
