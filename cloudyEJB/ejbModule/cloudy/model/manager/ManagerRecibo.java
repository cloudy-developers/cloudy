package cloudy.model.manager;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.Empresa;
import cloudy.model.entities.UsuaUsuario;
import cloudy.model.entities.VenDetalle;
import cloudy.model.entities.VenFormaPago;
import cloudy.model.entities.VenRecibo;

/**
 * Session Bean implementation class ManagerRecibo
 */
@Stateless
@LocalBean
public class ManagerRecibo 
{
	
    @PersistenceContext
    EntityManager em;
    
    public ManagerRecibo() {}
    
    public List<VenRecibo> findAllRecibos()
    {
    	String consulta = "SELECT v FROM VenRecibo v";
    	Query q = em.createQuery(consulta, VenRecibo.class);
    	return q.getResultList();
    }
    
    public void CrearRecibo(Empresa empresa, UsuaUsuario usuario, VenFormaPago formapago, VenRecibo recibo)
    {
    	VenRecibo nuevoRecibo = new VenRecibo();
    	
    	nuevoRecibo.setEmpresa(empresa);
    	nuevoRecibo.setVenFormaPago(formapago);
    	
    	String codigo = String.valueOf(Math.floor(Math.random()*(10000-5+1)+5));
    	
    	nuevoRecibo.setCodigoRecibo(codigo);
    	if(usuario!=null)
    	{
    		nuevoRecibo.setUsuaUsuario(usuario);
    	}
    	
    	nuevoRecibo.setNombreCliente(recibo.getNombreCliente());
    	nuevoRecibo.setCedulaCliente(recibo.getCedulaCliente());
    	nuevoRecibo.setDireccionCliente(recibo.getDireccionCliente());
    	nuevoRecibo.setTelefonoCliente(recibo.getTelefonoCliente());
    	nuevoRecibo.setCorreoCliente(recibo.getCorreoCliente());
    	
    	java.util.Date fecha = new Date();
    	nuevoRecibo.setFechaRecibo(fecha);
    	
    	nuevoRecibo.setSubtotal(BigDecimal.valueOf(0));
    	nuevoRecibo.setBaseImponible(BigDecimal.valueOf(0));
    	nuevoRecibo.setBaseCero(BigDecimal.valueOf(0));
    	nuevoRecibo.setDescuento(BigDecimal.valueOf(0));
    	nuevoRecibo.setTotalPago(BigDecimal.valueOf(0));
    	
    	em.persist(nuevoRecibo);
    }
     
    public List<VenRecibo> findAllRecibosByCliente(UsuaUsuario cliente)
    {
    	String consulta = "SELECT v FROM VenRecibo v WHERE v.usuaUsuario.idusuario = "+cliente.getIdusuario();
    	Query q = em.createQuery(consulta, VenRecibo.class);
    	return q.getResultList();
    }
    
    public double calcularBaseImponible(List<VenDetalle> detalle)
    {
    	double resultado = 0;
    	for (int i = 0; i < detalle.size(); i++)
    	{
			if(detalle.get(i).getInvProducto().getVenIva().getIva().doubleValue()!=0)
			{
				resultado = resultado + (detalle.get(i).getCantidad().doubleValue())*(detalle.get(i).getValorProducto().doubleValue());
			}
		}
    	return resultado;
    }
    
    public double calcularBaseCero(List<VenDetalle> detalle)
    {
    	double resultado = 0;
    	for (int i = 0; i < detalle.size(); i++)
    	{
			if(detalle.get(i).getInvProducto().getVenIva().getIva().doubleValue()==0)
			{
				resultado = resultado + (detalle.get(i).getCantidad().doubleValue())*(detalle.get(i).getValorProducto().doubleValue());
			}
		}
    	return resultado;
    }
    
    public void ActualizarValoresRecibo(VenRecibo recibo, BigDecimal baseimponible, BigDecimal basecero, BigDecimal subtotal)
    {
    	Double total = baseimponible.doubleValue()+basecero.doubleValue()+subtotal.doubleValue();
    	recibo.setBaseCero(basecero);
    	recibo.setBaseImponible(baseimponible);
    	recibo.setSubtotal(subtotal);
    	recibo.setTotalPago(BigDecimal.valueOf(total));
    	em.merge(recibo);
    }

}
