package cloudy.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.UsuaRole;

/**
 * Session Bean implementation class ManagerRoles
 */
@Stateless
@LocalBean
public class ManagerRoles 
{
	@PersistenceContext
	EntityManager em;

    public ManagerRoles() {}
    
    public List<UsuaRole> findAllRoles()
    {
    	String consulta = "SELECT d FROM UsuaRole d";
    	Query q = em.createQuery(consulta, UsuaRole.class);
    	return q.getResultList();
    }
    
    public UsuaRole FindAdministrador()
    {
    	UsuaRole admin = em.find(UsuaRole.class, 1);
    	return admin;
    }
    
    public UsuaRole FindVendedor()
    {
    	UsuaRole vendedor = em.find(UsuaRole.class, 2);
    	return vendedor;
    }
    
    public UsuaRole FindUsuario()
    {
    	UsuaRole usuario = em.find(UsuaRole.class, 3);
    	return usuario;
    }
    public UsuaRole findByIdRol(int idRol) {
        return (UsuaRole) em.find(UsuaRole.class, idRol);
    }
}
