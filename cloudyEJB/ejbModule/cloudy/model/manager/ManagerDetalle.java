package cloudy.model.manager;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.InvProducto;
import cloudy.model.entities.VenDetalle;
import cloudy.model.entities.VenIva;
import cloudy.model.entities.VenRecibo;


@Stateless
@LocalBean
public class ManagerDetalle 
{
	@PersistenceContext
	EntityManager em;

    public ManagerDetalle() {}

    public void IngresarDetalle(VenRecibo recibo, InvProducto producto, BigDecimal cantidad, BigDecimal valor, VenIva iva)
    {
    	VenDetalle nuevoDetalle = new VenDetalle();
    	nuevoDetalle.setVenRecibo(recibo);
    	nuevoDetalle.setInvProducto(producto);
    	nuevoDetalle.setCantidad(cantidad);
    	nuevoDetalle.setValorProducto(valor);
    	nuevoDetalle.setVenIva(iva);
    	em.persist(nuevoDetalle);
    }
    
    public List<VenDetalle> FindAllDetalles()
    {
    	String consulta = "SELECT v FROM VenDetalle v";
    	Query q = em.createQuery(consulta, VenDetalle.class);
    	return q.getResultList();
    }
    
    public List<VenDetalle> FindAllDetallesByRecibo(VenRecibo recibo)
    {
    	String consulta = "SELECT v FROM VenDetalle v WHERE v.venRecibo.idrecibo = "+recibo.getIdrecibo();
    	Query q = em.createQuery(consulta, VenDetalle.class);
    	return q.getResultList();
    }
    
    
}
