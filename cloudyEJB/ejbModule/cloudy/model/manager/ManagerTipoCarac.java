package cloudy.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.InvTipoCaracteristica;

/**
 * Session Bean implementation class ManagerTipoCarac
 */
@Stateless
@LocalBean
public class ManagerTipoCarac 
{
	@PersistenceContext
	EntityManager em;

    public ManagerTipoCarac() {}
    
    public List<InvTipoCaracteristica> findAllTiposdeCaracteristicas()
    {
    	String consulta = "SELECT i FROM InvTipoCaracteristica i";
    	Query q= em.createQuery(consulta, InvTipoCaracteristica.class);
    	return q.getResultList();
    }
    
    //--------------- CARACTERTTICAS DE LOS MODS -----------------------
    
    public InvTipoCaracteristica findTipoMod()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 1);
    	return tipoMod;
    }
    
    public InvTipoCaracteristica findTipoBateriaMod()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 2);
    	return tipoMod;
    }
    
    public InvTipoCaracteristica findBottomFeederMod()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 3);
    	return tipoMod;
    }
    
    public InvTipoCaracteristica findTipoRoscaMod()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 4);
    	return tipoMod;
    }
    
    //----------------- CARACTERISTICAS DE LOS ATOMIZADORES ------------------
    
    public InvTipoCaracteristica findTipoAtomizador()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 5);
    	return tipoMod;
    }
    
    public InvTipoCaracteristica findTamanoAtomizador()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 6);
    	return tipoMod;
    }
    
    public InvTipoCaracteristica findBottomFeederAtomizador()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 7);
    	return tipoMod;
    }
    
    public InvTipoCaracteristica findTipoRoscaAtomizador()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 8);
    	return tipoMod;
    }
    
    //-------------------- CARACTERISTICAS DE LAS RESISTENCIAS --------
    
    public InvTipoCaracteristica findTipoResistencia()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 9);
    	return tipoMod;
    }
    
    public InvTipoCaracteristica findTipoRangoMaximoResistencia()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 10);
    	return tipoMod;
    }
    
    public InvTipoCaracteristica findTipoRangoMinimoResistencia()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 11);
    	return tipoMod;
    }
    
    public InvTipoCaracteristica findTipoOhmiosResistencia()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 12);
    	return tipoMod;
    }
    
    //---------------- CARACTERISTICAS DE LOS ALGODONES ------------------
    
    public InvTipoCaracteristica findTipoAlgodon()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 13);
    	return tipoMod;
    }
    
    //---------------- CARACTERISTICAS DE LAS ESENCIAS -----------------------
    
    public InvTipoCaracteristica findTipoSaborEsencia()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 14);
    	return tipoMod;
    }
    
    public InvTipoCaracteristica findTipoNicotinaEsencia()
    {
    	InvTipoCaracteristica tipoMod = em.find(InvTipoCaracteristica.class, 15);
    	return tipoMod;
    }

}
