package cloudy.model.manager;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.InvProducto;
import cloudy.model.entities.InvProductoCaracteristica;
import cloudy.model.entities.InvValorEstandar;

/**
 * Session Bean implementation class ManagerValorEstandar
 */
@Stateless
@LocalBean
public class ManagerValorEstandar {
	@PersistenceContext
	EntityManager em;
    
	public double suma;
    public ManagerValorEstandar() {
        // TODO Auto-generated constructor stub
    }
    
    public List<InvValorEstandar> findAllValor(){
    	Query q  = em.createQuery("SELECT i FROM InvValorEstandar i", InvValorEstandar.class);
    	return q.getResultList();
    }
    
   public double findValorEstandar(List<InvValorEstandar> valorest,int idprod){
	   double valor=0;
	   for(InvValorEstandar p:valorest) {
		   if(p.getInvProducto().getIdproducto()==idprod) {
			 valor=p.getValorEstandar().doubleValue();  
		   }
	   }
	   return valor;
    }
   
   public double SumaCarrito(List<InvValorEstandar> valorest,int idprod) {
	 
	   
	   for(InvValorEstandar p:valorest) {
		   if(p.getInvProducto().getIdproducto()==idprod) {
			 suma=suma+Double.parseDouble(p.getValorEstandar().toString());  
		   }
	   
	   }
	   System.out.println("valor suma prod:"+suma);
	   return suma;
   }
   
public double RestaCarrito(List<InvValorEstandar> valorest,int idprod) {
	 
	   
	   for(InvValorEstandar p:valorest) {
		   if(p.getInvProducto().getIdproducto()==idprod) {
			 suma=suma-Double.parseDouble(p.getValorEstandar().toString());  
		   }
	   
	   }
	   System.out.println("valor suma prod:"+suma);
	   return suma;
   }
   
   //----------------------- METODOS MILOS ---------------------------------------------------------
   
   public void IngresarValor(InvValorEstandar valor, InvProducto producto)
   {
   		InvValorEstandar nuevovalor = new InvValorEstandar();
   	
   		nuevovalor.setInvProducto(producto);
   		nuevovalor.setValorEstandar(valor.getValorEstandar());
       	
   		em.persist(nuevovalor);
   }
   
   public void ActualizarValor(InvValorEstandar valor, BigDecimal valorproducto)
   {
	   valor.setValorEstandar(valorproducto);
	   em.merge(valor);
   }
   
   public InvValorEstandar findValorEstandarByProducto(List<InvValorEstandar> valorest, InvProducto producto)
   {
	   InvValorEstandar p = new InvValorEstandar();
	   for (int i = 0; i < valorest.size(); i++) 
	   {
		   if(valorest.get(i).getInvProducto().getIdproducto()==producto.getIdproducto())
		   {
			   p=valorest.get(i);
			   break;
		   }
	   }
	   return p;
    }

}
