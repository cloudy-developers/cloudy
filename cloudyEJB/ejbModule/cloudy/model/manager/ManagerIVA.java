package cloudy.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.VenIva;

/**
 * Session Bean implementation class ManagerIVA
 */
@Stateless
@LocalBean
public class ManagerIVA 
{
	@PersistenceContext
	EntityManager em;

    public ManagerIVA() {}
    
    public List<VenIva> findAllIVA()
    {
    	String consulta = "SELECT v FROM VenIva v";
    	Query q = em.createQuery(consulta, VenIva.class);
    	return q.getResultList();
    }
    
    public VenIva findIVAbyID(int ID)
    {
    	VenIva iva = em.find(VenIva.class, ID);
    	return iva;
    }
    
    public void IngresarIva(VenIva iva)
    {
    	VenIva nuevoiva = new VenIva();
    	nuevoiva.setIva(iva.getIva());
    	em.persist(nuevoiva);
    }

}
