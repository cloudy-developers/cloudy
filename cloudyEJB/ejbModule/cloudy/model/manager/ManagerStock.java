package cloudy.model.manager;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.InvProducto;
import cloudy.model.entities.InvProductoCaracteristica;
import cloudy.model.entities.InvStock;

/**
 * Session Bean implementation class ManagerStock
 */
@Stateless
@LocalBean
public class ManagerStock 
{
	@PersistenceContext
	EntityManager em;

    public ManagerStock() {}
    
    public List<InvStock> findAllStock()
    {
    	String consulta = "SELECT i FROM InvStock i";
    	Query q = em.createQuery(consulta, InvStock.class);
    	return q.getResultList();
    }
    
    public void IngresarProductoAlStock(InvProducto producto, InvStock stock)
    {
    	InvStock nuevoStock = new InvStock();
    	
    	nuevoStock.setInvProducto(producto);
    	nuevoStock.setCantidadEnAlmacen(0);
    	nuevoStock.setDescripcion(stock.getDescripcion());
    	nuevoStock.setMaximo(stock.getMaximo());
    	nuevoStock.setMinimo(stock.getMinimo());
    	
    	em.persist(nuevoStock);
    }
    
    public InvStock FindStockByProducto(List<InvStock> listaStock, InvProducto producto)
    {
    	InvStock stock = new InvStock();
    	for (int i = 0; i < listaStock.size(); i++)
    	{
			if(listaStock.get(i).getInvProducto()==producto)
			{
				stock = listaStock.get(i);
				break;
			}
		}
    	return stock;
    }
    
    public void ActualizarStock(InvStock stock, int cantidad)
    {
    	stock.setCantidadEnAlmacen(stock.getCantidadEnAlmacen()+cantidad);
    	em.merge(stock);
    }
    
    /////////METODOS CHULDECILLO PILLO/////////
    public List<InvStock> agregarProductoCarrito(List<InvStock> carrito,InvStock p)
    {
    	if(carrito==null)
    		carrito=new ArrayList<InvStock>();
    	carrito.add(p);
    	return carrito;
    }
    
    public List<InvStock> eliminarProductoCarrito(List<InvStock> carrito,int codigoProductoCaracteristica)
    {
    	if(carrito==null)
    		return null;
    	int i=0;
    	for(InvStock p:carrito) {
    		if(p.getInvProducto().getIdproducto()==codigoProductoCaracteristica) {
    			carrito.remove(i);
    			break;
    		}
    		i++;
    	}
    	return carrito;
    }
}
