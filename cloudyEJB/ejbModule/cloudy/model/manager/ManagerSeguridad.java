package cloudy.model.manager;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import cloudy.model.dto.LoginDTO;
import cloudy.model.entities.UsuaUsuario;

/**
 * Session Bean implementation class ManagerSeguridad
 */
@Stateless
@LocalBean
public class ManagerSeguridad {

    /**
     * Default constructor. 
     */
	@EJB
	private ManagerDAO managerDAO;
	@EJB
	private ManagerAuditoria managerAuditoria;
	@EJB
	private ManagerUsuario managerUsuario;
	
    public ManagerSeguridad() {
        // TODO Auto-generated constructor stub
    }
    
    /**
	 * Metodo que le permite a un usuario acceder al sistema.
	 * @param codigoUsuario Identificador del usuario.
	 * @param clave Clave de acceso.
	 * @return Objeto LoginDTO con informacion del usuario para la sesion.
	 * @throws Exception Cuando no coincide la clave proporcionada o si ocurrio
	 * un error con la consulta a la base de datos.
	 */
	public LoginDTO accederSistema(String email,String clave) throws Exception{
		//UsuaUsuario usuario=(UsuaUsuario)managerDAO.findById(UsuaUsuario.class, idUsuario);		
		System.out.println("------- Email:"+email);
		UsuaUsuario usuario=(UsuaUsuario)managerUsuario.FindByEmail(email);
		if(usuario==null)
			throw new Exception("Error en usuario y/o clave.");
		
		if(!usuario.getContrasena().equals(clave))
			throw new Exception("Error en usuario y/o clave.");
		
		LoginDTO loginDTO=new LoginDTO();
		
		loginDTO.setIdusuario(usuario.getIdusuario());
		loginDTO.setNombreusuario(usuario.getNombre());
		loginDTO.setApellidousuario(usuario.getApellido());
		loginDTO.setCorreo(usuario.getEmail());
		loginDTO.setTelefono(usuario.getTelefono());
		loginDTO.setIddpa(usuario.getDpa().getIddivpolitica());
		loginDTO.setContrasenia(usuario.getContrasena());
		loginDTO.setTipoUsuario(usuario.getUsuaRole().getNomrol());		

		
		//dependiendo del tipo de usuario, configuramos la ruta de acceso a las pags web:
		if(usuario.getUsuaRole().getNomrol().equals("Administrador")) {
			loginDTO.setRutaAcceso("/administrador/Admin-principal.xhtml");
			}else 
			{if(usuario.getUsuaRole().getNomrol().equals("Vendedor")) {
			loginDTO.setRutaAcceso("/vendedor/vendedor-principal.xhtml");
			}else {
				if(usuario.getUsuaRole().getNomrol().equals("Cliente"))
			loginDTO.setRutaAcceso("/cliente/cliente-principal.xhtml");
				}
			}
		managerAuditoria.crearEvento(usuario.getIdusuario(), ManagerSeguridad.class, "accederSistema", "Acceso al sistema para usuarios.");
		return loginDTO;
	}
	
	
	public UsuaUsuario findUsuarioById(int idUsuario) throws Exception {
		UsuaUsuario usuario=(UsuaUsuario)managerDAO.findById(UsuaUsuario.class, idUsuario);
		return usuario;
	}

}
