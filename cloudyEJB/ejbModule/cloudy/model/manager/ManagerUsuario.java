package cloudy.model.manager;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.Dpa;
import cloudy.model.entities.UsuaRole;
import cloudy.model.entities.UsuaUsuario;


/**
 * Session Bean implementation class ManagerUsuario
 */
@Stateless
@LocalBean
public class ManagerUsuario 
{
	@PersistenceContext
	EntityManager em;

	@EJB
	private ManagerDAO managerDAO;
	
    public ManagerUsuario() {}
    
    /**
  	 * Metodo finder para la consulta de clientes.
  	 * Hace uso del componente {@link facturacion.model.manager.ManagerDAO ManagerDAO} de la capa model.
  	 * @return listado de clientes ordenados por apellidos.
  	 */
  	@SuppressWarnings("unchecked")
  	public List<UsuaUsuario> findAllClientes(){
  		return managerDAO.findAll(UsuaUsuario.class, "apellidos");
  	}
  	
  	/**
  	 * Metodo finder para la consulta de un cliente especifico.
  	 * @param cedula cedula del cliente que se desea buscar.
  	 * @return datos del cliente.
  	 * @throws Exception
  	 */
  	public UsuaUsuario findClienteById(String cedula) throws Exception{
  		UsuaUsuario cliente=null;
  		try {
  			cliente=(UsuaUsuario)managerDAO.findById(UsuaUsuario.class, cedula);
  		} catch (Exception e) {
  			e.printStackTrace();
  			throw new Exception("Error al buscar cliente: "+e.getMessage());
  		}
  		return cliente;
  	}
  	
    public List<UsuaUsuario> findAllUsuario()
    {
    	String consulta = "SELECT u FROM UsuaUsuario u";
    	Query q = em.createQuery(consulta, UsuaUsuario.class);
    	return q.getResultList();
    }
    
    public UsuaUsuario findByEmail2(String email)
    {
    	return em.find(UsuaUsuario.class, email);
    }
    
    public List<UsuaUsuario> findPerfilByIdUsuario (int idUsuario)
    {
        Query q=em.createQuery("SELECT u FROM UsuaUsuario u where u.idusuario="+idUsuario, UsuaUsuario.class);
        return q.getResultList();
    }
    
    public UsuaUsuario findByIdUsuario(int idUsuario) 
    {
    	return (UsuaUsuario) em.find(UsuaUsuario.class, idUsuario);
    }
    
    public void NewVendedor(UsuaUsuario usuario, UsuaRole rolVendedor, Dpa direccion)
    {
    	UsuaUsuario nuevoUsuario = new UsuaUsuario();
    	nuevoUsuario.setUsuaRole(rolVendedor);
    	nuevoUsuario.setDpa(direccion);
    	nuevoUsuario.setNombre(usuario.getNombre());
    	nuevoUsuario.setApellido(usuario.getApellido());
    	nuevoUsuario.setEmail(usuario.getEmail());
    	nuevoUsuario.setTelefono(usuario.getTelefono());
    	nuevoUsuario.setContrasena(usuario.getContrasena());
    	
    	em.persist(nuevoUsuario);
    }
    
    public void NewUsuario(UsuaUsuario usuario, UsuaRole rolCliente, Dpa direccion)
    {
    	UsuaUsuario nuevoUsuario = new UsuaUsuario();
    	nuevoUsuario.setUsuaRole(rolCliente);
    	nuevoUsuario.setDpa(direccion);
    	nuevoUsuario.setNombre(usuario.getNombre());
    	nuevoUsuario.setApellido(usuario.getApellido());
    	nuevoUsuario.setEmail(usuario.getEmail());
    	nuevoUsuario.setTelefono(usuario.getTelefono());
    	nuevoUsuario.setContrasena(usuario.getContrasena());
    	
    	em.persist(nuevoUsuario);
    }
    
    public void actualizarUsuario(UsuaUsuario u) {
    	UsuaUsuario a = null;
    	a=findByIdUsuario(u.getIdusuario());
    	
    	a.setNombre(u.getNombre());
    	a.setApellido(u.getApellido());
    	a.setEmail(u.getEmail());
    	a.setTelefono(u.getTelefono());
    	a.setContrasena(u.getContrasena());
    	a.setDpa(u.getDpa());
    
    	em.merge(a);
    }
    

    public UsuaUsuario FindByEmail(String email) {
    	UsuaUsuario usuario = new UsuaUsuario();
    	List<UsuaUsuario> listadeUsuarios = this.findAllUsuario();
    	for (int i = 0; i < listadeUsuarios.size(); i++) {
    		if (listadeUsuarios.get(i).getEmail().equals(email)) {
				usuario = listadeUsuarios.get(i);
				break;
			}
    	}
    	return usuario;
    }
    
    public List<UsuaUsuario> AllClientes()
    {
    	String consulta = "select a from UsuaUsuario a, UsuaRole b where a.usuaRole.idrol = b.idrol and b.nomrol = 'Cliente'";
    	Query q = em.createQuery(consulta, UsuaUsuario.class);
    	return q.getResultList();
    }
    
    public List<UsuaUsuario> AllVendedores()
    {
    	String consulta = "select a from UsuaUsuario a, UsuaRole b where a.usuaRole.idrol = b.idrol and b.nomrol = 'Vendedor'";
    	Query q = em.createQuery(consulta, UsuaUsuario.class);
    	return q.getResultList();
    }


}
