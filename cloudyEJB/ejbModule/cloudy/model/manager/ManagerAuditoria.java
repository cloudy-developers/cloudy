package cloudy.model.manager;

import java.net.InetAddress;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import cloudy.model.entities.Bitacora;

/**
 * Session Bean implementation class ManagerAuditoria
 */
@Stateless
@LocalBean
public class ManagerAuditoria {

    /**
     * Default constructor. 
     */
	@EJB
	private ManagerDAO managerDAO;
	@EJB
	private ManagerSeguridad managerSeguridad;
	
    public ManagerAuditoria() {
        // TODO Auto-generated constructor stub
    }

    /**
	 * Almacena la informacion de un evento en la tabla de auditoria.
	 * @param codigoUsuario Codigo del usuario que genero el evento.
	 * @param clase Clase que genera el evento.
	 * @param metodo Nombre del metodo que genero el evento.
	 * @param descripcion Informacion detallada del evento.
	 * @throws Exception 
	 */
	public void crearEvento(int codigoUsuario,Class clase,String metodo,String descripcion) throws Exception{
		Bitacora evento=new Bitacora();
		Integer idUsuario= new Integer(codigoUsuario);
		//cambio para probar git
		
		//Direccion IP
		InetAddress direccioIp = InetAddress.getLocalHost();
		String ipLocal = direccioIp.getHostAddress();
		
		if(idUsuario==null)
			throw new Exception("Error auditoria: debe indicar el codigo del usuario.");
		if(metodo==null||metodo.length()==0)
			throw new Exception("Error auditoria: debe indicar el metodo que genera el evento.");

		evento.setCodigoUsuario(codigoUsuario);
		evento.setMetodo(clase.getSimpleName()+"/"+metodo);
		evento.setDescripcion(descripcion);
		evento.setFechaEvento(new Date());
		evento.setDireccionIp(ipLocal);
		
		managerDAO.insertar(evento);
	}
	
	public List<Bitacora> findAllBitacoras(){
		return managerDAO.findAll(Bitacora.class, "codigoUsuario");
	}
}
