package cloudy.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.InvTipoProducto;

/**
 * Session Bean implementation class ManagerTipoProducto
 */
@Stateless
@LocalBean
public class ManagerTipoProducto 
{
	@PersistenceContext
	EntityManager em;

    public ManagerTipoProducto() {}
    
    public List<InvTipoProducto> findAllTiposdeProductos()
    {
    	String consulta = "SELECT i FROM InvTipoProducto i";
    	Query q = em.createQuery(consulta, InvTipoProducto.class);
    	return q.getResultList();
    }
    
    public InvTipoProducto FindTipoProductoByID(int idtipoproducto)
    {
    	InvTipoProducto tipoproducto = em.find(InvTipoProducto.class, idtipoproducto);
    	return tipoproducto;
    }
    
    public void INgresarNuevoTipoDeProducto(InvTipoProducto tproducto)
    {
    	InvTipoProducto nuevotipoProducto = new InvTipoProducto();
    	nuevotipoProducto.setNomtipoproducto(tproducto.getNomtipoproducto());
    	em.persist(nuevotipoProducto);
    }

}
