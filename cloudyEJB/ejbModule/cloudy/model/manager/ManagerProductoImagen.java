package cloudy.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.InvProducto;
import cloudy.model.entities.InvProductoImagen;

@Stateless
@LocalBean
public class ManagerProductoImagen 
{
	@PersistenceContext
	EntityManager em;

    public ManagerProductoImagen() {}
    
    public void IngresarProductoImagen(InvProducto producto, InvProductoImagen prodImg)
    {
    	InvProductoImagen nuevoprodIMG = new InvProductoImagen();
    	nuevoprodIMG.setDireccionImagen(prodImg.getDireccionImagen());
    	nuevoprodIMG.setInvProducto(producto);
    	em.persist(nuevoprodIMG);
    }
    
    public List<InvProductoImagen> FindAllImagenesProducto()
    {
    	String consulta = "SELECT i FROM InvProductoImagen i";
    	Query q = em.createQuery(consulta, InvProductoImagen.class);
    	return q.getResultList();
    }
    
    public InvProductoImagen FindImagenByProducto(InvProducto producto)
    {
    	InvProductoImagen img = null;
    	List<InvProductoImagen> imagenes = this.FindAllImagenesProducto();
    	for (int i = 0; i < imagenes.size(); i++)
    	{
			if(producto==imagenes.get(i).getInvProducto())
			{
				img=imagenes.get(i);
			}
		}
    	return img;
    }
    
    public List<InvProductoImagen> findImagenesDeProductos(InvProducto producto)
    {
    	String consulta = "SELECT i FROM InvProductoImagen i WHERE i.invProducto.nombreProducto = '"+producto.getNombreProducto()+"'";
    	Query q = em.createQuery(consulta, InvProductoImagen.class);
    	return q.getResultList();
    }

}
