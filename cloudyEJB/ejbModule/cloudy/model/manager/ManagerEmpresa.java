package cloudy.model.manager;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import cloudy.model.dto.EmpresaDTO;
import cloudy.model.entities.Empresa;

@Stateless
@LocalBean
public class ManagerEmpresa 
{
	@PersistenceContext
	EntityManager em;

    public ManagerEmpresa() {}
    
    public Empresa FindEmpresaByID(Integer id)
    {
    	Empresa emp = em.find(Empresa.class, id);
    	return emp;
    }
    
    public List<EmpresaDTO> findAllEmpresas()
    {
    	List<EmpresaDTO> lista=new ArrayList<EmpresaDTO>();
    	for(Empresa e:em.createNamedQuery("Empresa.findAll", Empresa.class).getResultList()) {
    		EmpresaDTO edto=new EmpresaDTO(e.getIdempresa(),e.getNombre(),e.getDireccion(),e.getCorreo(),e.getCanton());
    		lista.add(edto);
    	}
    	return lista;
    }
    
    public Empresa findEmpresaById(int idEmpresa) {
    	Empresa e=em.find(Empresa.class, idEmpresa);
    	return e;
    }
    
    public EmpresaDTO findAllEmpresaDTOById(int idEmpresa) {
    	EmpresaDTO edto = null;
    	for(Empresa e:em.createNamedQuery("Empresa.findAll", Empresa.class).getResultList()) {
    		if (e.getIdempresa()==idEmpresa) {
				edto = new EmpresaDTO(e.getIdempresa(),e.getNombre(),e.getDireccion(),e.getCorreo(),e.getCanton());
				break;
			}
    	}
    	return edto;
    }
    
    public void InsertarEmpresa(Empresa empresa)
    {
    	Empresa nuevaEmpresa = new Empresa();
    	
    	nuevaEmpresa.setNombre(empresa.getNombre());
    	nuevaEmpresa.setDireccion(empresa.getDireccion());
    	nuevaEmpresa.setCorreo(empresa.getCorreo());
    	nuevaEmpresa.setCanton(empresa.getCanton());
    	
    	em.persist(nuevaEmpresa);
    }
    
    public void eliminarEmpresa(int idEmpresa) {
    	Empresa e = em.find(Empresa.class, idEmpresa);
    	em.remove(e);
    }
    
    
    
    public void actualizarEmpresaById(int idEmpresa, Empresa empresa) {
    	Empresa e = findEmpresaById(idEmpresa);
    	
    	e.setNombre(empresa.getNombre());
    	e.setDireccion(empresa.getDireccion());
    	e.setCorreo(empresa.getCorreo());
    	e.setCanton(empresa.getCanton());
    
    	em.merge(e);
    }


}
