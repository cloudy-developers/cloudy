package cloudy.model.manager;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cloudy.model.entities.InvProducto;
import cloudy.model.entities.InvProductoCaracteristica;
import cloudy.model.entities.InvTipoCaracteristica;

/**
 * Session Bean implementation class ManagerProductos
 */
@Stateless
@LocalBean
public class ManagerProductoCaracteristica {
	@PersistenceContext
	EntityManager em;
    
    public ManagerProductoCaracteristica() {}
    
    public void IngresarProductoCaracteristica(InvProducto producto, InvTipoCaracteristica tipocaracteristica, InvProductoCaracteristica productoCaracteristica)
    {
    	productoCaracteristica.setInvProducto(producto);
    	productoCaracteristica.setInvTipoCaracteristica(tipocaracteristica);
    	em.persist(productoCaracteristica);
    }
    
    //------------------- METODOS CHULDECILLO PILLO ---------------------------//
    
    public InvProducto findProductobyId(int idproducto){
    	return em.find(InvProducto.class, idproducto);
    }
    
    public List<InvProductoCaracteristica> findAllProductos(){
    	Query q  = em.createQuery("SELECT i FROM InvProductoCaracteristica i", InvProductoCaracteristica.class);
    	return q.getResultList();
    }
  
    public List<InvProductoCaracteristica> agregarProductoCarrito(List<InvProductoCaracteristica> carrito,InvProductoCaracteristica p){
    	if(carrito==null)
    		carrito=new ArrayList<InvProductoCaracteristica>();
    	carrito.add(p);
    	return carrito;
    }
    public List<InvProductoCaracteristica> eliminarProductoCarrito(List<InvProductoCaracteristica> carrito,int codigoProductoCaracteristica){
    	if(carrito==null)
    		return null;
    	int i=0;
    	for(InvProductoCaracteristica p:carrito) {
    		if(p.getIdcaracteristica()==codigoProductoCaracteristica) {
    			carrito.remove(i);
    			break;
    		}
    		i++;
    	}
    	return carrito;
    }
    
    //------------------ METODOS RICARDO MILOS ------------------
    
    public void IngresarModORAtomizadorORResistencia(InvProducto producto, InvTipoCaracteristica tipoMod, String descTipoMod, InvTipoCaracteristica tipoBateria, String descBateria, InvTipoCaracteristica bottomfeeder, String descBottom, InvTipoCaracteristica rosca, String descRosca)
    {
    	InvProductoCaracteristica caracterTipoMod = new InvProductoCaracteristica();
    	caracterTipoMod.setInvProducto(producto);
    	caracterTipoMod.setInvTipoCaracteristica(tipoMod);
    	caracterTipoMod.setDescripcionCaracteristica(descTipoMod);
    	em.persist(caracterTipoMod);
    	
    	InvProductoCaracteristica caracterTipoBateria = new InvProductoCaracteristica();
    	caracterTipoBateria.setInvProducto(producto);
    	caracterTipoBateria.setInvTipoCaracteristica(tipoBateria);
    	caracterTipoBateria.setDescripcionCaracteristica(descBateria);
    	em.persist(caracterTipoBateria);
    	
    	InvProductoCaracteristica caracterbottom = new InvProductoCaracteristica();
    	caracterbottom.setInvProducto(producto);
    	caracterbottom.setInvTipoCaracteristica(bottomfeeder);
    	caracterbottom.setDescripcionCaracteristica(descBottom);
    	em.persist(caracterbottom);
    	
    	InvProductoCaracteristica caracterrosca = new InvProductoCaracteristica();
    	caracterrosca.setInvProducto(producto);
    	caracterrosca.setInvTipoCaracteristica(rosca);
    	caracterrosca.setDescripcionCaracteristica(descRosca);
    	em.persist(caracterrosca);
    }
    
    public void IngresarAlgodon(InvProducto algodon, InvTipoCaracteristica tiopoAlgodon, String descAlgodon)
    {
    	InvProductoCaracteristica caracterAlgodon = new InvProductoCaracteristica();
    	caracterAlgodon.setInvProducto(algodon);
    	caracterAlgodon.setInvTipoCaracteristica(tiopoAlgodon);
    	caracterAlgodon.setDescripcionCaracteristica(descAlgodon);
    	em.persist(caracterAlgodon);
    }
    
    public void IngresarEsencia(InvProducto esencia, InvTipoCaracteristica sabor, String descsabor, InvTipoCaracteristica nicotina, String descnicotina)
    {
    	InvProductoCaracteristica caracterEsencia = new InvProductoCaracteristica();
    	caracterEsencia.setInvProducto(esencia);
    	caracterEsencia.setInvTipoCaracteristica(sabor);
    	caracterEsencia.setDescripcionCaracteristica(descsabor);
    	em.persist(caracterEsencia);
    	
    	InvProductoCaracteristica caracterNicotina = new InvProductoCaracteristica();
    	caracterNicotina.setInvProducto(esencia);
    	caracterNicotina.setInvTipoCaracteristica(nicotina);
    	caracterEsencia.setDescripcionCaracteristica(descnicotina);
    	em.persist(caracterEsencia);
    }
}
