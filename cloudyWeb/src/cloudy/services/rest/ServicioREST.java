package cloudy.services.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import cloudy.model.dto.EmpresaDTO;
import cloudy.model.entities.Empresa;
import cloudy.model.manager.ManagerEmpresa;

@RequestScoped
@Path("/cloudy")
@Produces("application/json")
@Consumes("application/json")
public class ServicioREST {


	@EJB
	private ManagerEmpresa mEmpresa;
	
	@GET
	@Path("/consultar")
	public List<EmpresaDTO> findAllEmpresas(){
		return mEmpresa.findAllEmpresas();
	}
	
	@GET
	@Path("/consultar/{idEmpresa}")
	public EmpresaDTO consultarPorId(@PathParam("idEmpresa") int idEmpresa) {
		return mEmpresa.findAllEmpresaDTOById(idEmpresa);
	}

	@POST
	@Path("/consultar")
	public String InsertarEmpresa(Empresa empresa) {
		try {
			mEmpresa.InsertarEmpresa(empresa);
			return "{\"resultado\":\"CORRECTO\"}";
		} catch (Exception e) {
			return "{\"resultado\":\"INCORRECTO\"}";
		}
	}
	
	@DELETE
	@Path("/consultar/{idEmpresa}")
	public String EliminarEmpresa(@PathParam("idEmpresa") int idEmpresa) {
		try {
			mEmpresa.eliminarEmpresa(idEmpresa);
			return "{\"resultado\":\"CORRECTO\"}";
		} catch (Exception e) {
			return "{\"resultado\":\"INCORRECTO\"}";
		}
	}
	
	@PUT
	@Path("/consultar/{idEmpresa}")
	public String ActualizarEmpresa(@PathParam("idEmpresa") int idEmpresa, Empresa empresa) {
		try {
			mEmpresa.actualizarEmpresaById(idEmpresa, empresa);
			return "{\"resultado\":\"CORRECTO\"}";
		} catch (Exception e) {
			return "{\"resultado\":\"INCORRECTO\"}";
		}
	}

	
}
