package cloudy.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import cloudy.model.entities.InvCompraProducto;
import cloudy.model.entities.InvProducto;
import cloudy.model.entities.InvStock;
import cloudy.model.entities.InvValorEstandar;
import cloudy.model.manager.ManagerCompraProducto;
import cloudy.model.manager.ManagerProducto;
import cloudy.model.manager.ManagerStock;
import cloudy.model.manager.ManagerValorEstandar;

import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class BeanStock implements Serializable 
{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ManagerStock managerStock;
	@EJB
	private ManagerProducto managerProducto;
	@EJB
	private ManagerValorEstandar mValorEstandar;
	@EJB
	private ManagerCompraProducto mCompraProducto;
	
	//--------------------OBJETOS------------------------
	private InvStock nuevoStock;
	private InvStock stockSelecionado;
	private InvProducto producto;
	private InvValorEstandar nuevoValorEstandar;
	private InvCompraProducto nuevacompra;
	
	//------------- VARIABLES ----------
	private int idProducto;
	
	//----------------LISTAS------------------------
	private List<InvStock> listaStock;
	private List<InvProducto> listaProductos;
	private List<InvProducto> listaProductosNoStock;
	private List<InvValorEstandar> listaValores;
	private List<InvCompraProducto> listadecompras;
	
	@PostConstruct
	public void Inicializar()
	{
		nuevoStock = new InvStock();
		producto = new InvProducto();
		nuevoValorEstandar = new InvValorEstandar();
		nuevacompra = new InvCompraProducto();
		
		listaProductos = managerProducto.findAllProductos();
		listaStock = managerStock.findAllStock();
		listaValores = mValorEstandar.findAllValor();
		listaProductosNoStock = managerProducto.findProductosNoStock();
	}
	
	public void actionIngresarProductoaStock()
	{
		producto=managerProducto.findByID(idProducto);
		
		managerStock.IngresarProductoAlStock(producto, nuevoStock);
		
		listaStock = managerStock.findAllStock();
		listaProductosNoStock = managerProducto.findProductosNoStock();
		nuevoStock = new InvStock();
		producto = new InvProducto();
	}
	
	public void actionListenerSeleccionarStock(InvStock stock)
	{
		stockSelecionado = stock;
	}
	
	public void actionComprarProductos()
	{
		mCompraProducto.IngresarCompra(nuevacompra, stockSelecionado.getInvProducto());
		
		nuevoStock = managerStock.FindStockByProducto(listaStock, stockSelecionado.getInvProducto());
		managerStock.ActualizarStock(nuevoStock, nuevacompra.getCantidadCompra().intValue());
		
		nuevacompra = new InvCompraProducto();
		nuevoStock = new InvStock();
	}
	
	public void actionDarValorProducto()
	{
		
		InvValorEstandar valor= new InvValorEstandar();
		valor=mValorEstandar.findValorEstandarByProducto(listaValores, stockSelecionado.getInvProducto());
		
		System.out.println("///////// EL ID DEL VALOR ES: "+valor.getIdvalorEstand());
		
		if(valor.getIdvalorEstand()==null)
		{
			System.out.println("/////////////El valor es nulo este esta correindo////////////");
			mValorEstandar.IngresarValor(nuevoValorEstandar, stockSelecionado.getInvProducto());
		}
		
		if(valor.getIdvalorEstand()!=null)
		{
			System.out.println("///////////////////El valor no es nulo ///////////////////");
			mValorEstandar.ActualizarValor(valor, nuevoValorEstandar.getValorEstandar());
		}
		
		listaValores = mValorEstandar.findAllValor();
		nuevoValorEstandar = new InvValorEstandar();
	}
	
	//-------------------- GETTERS Y SETTERS ----------------------------
	public InvStock getNuevoStock() {
		return nuevoStock;
	}
	public void setNuevoStock(InvStock nuevoStock) {
		this.nuevoStock = nuevoStock;
	}
	public InvProducto getProducto() {
		return producto;
	}
	public void setProducto(InvProducto producto) {
		this.producto = producto;
	}
	public List<InvStock> getListaStock() {
		return listaStock;
	}
	public void setListaStock(List<InvStock> listaStock) {
		this.listaStock = listaStock;
	}
	public List<InvProducto> getListaProductos() {
		return listaProductos;
	}
	public void setListaProductos(List<InvProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public InvStock getStockSelecionado() {
		return stockSelecionado;
	}

	public void setStockSelecionado(InvStock stockSelecionado) {
		this.stockSelecionado = stockSelecionado;
	}

	public InvValorEstandar getNuevoValorEstandar() {
		return nuevoValorEstandar;
	}

	public void setNuevoValorEstandar(InvValorEstandar nuevoValorEstandar) {
		this.nuevoValorEstandar = nuevoValorEstandar;
	}

	public InvCompraProducto getNuevacompra() {
		return nuevacompra;
	}

	public void setNuevacompra(InvCompraProducto nuevacompra) {
		this.nuevacompra = nuevacompra;
	}

	public List<InvValorEstandar> getListaValores() {
		return listaValores;
	}

	public void setListaValores(List<InvValorEstandar> listaValores) {
		this.listaValores = listaValores;
	}

	public List<InvCompraProducto> getListadecompras() {
		return listadecompras;
	}

	public void setListadecompras(List<InvCompraProducto> listadecompras) {
		this.listadecompras = listadecompras;
	}

	public List<InvProducto> getListaProductosNoStock() {
		return listaProductosNoStock;
	}

	public void setListaProductosNoStock(List<InvProducto> listaProductosNoStock) {
		this.listaProductosNoStock = listaProductosNoStock;
	}
}
