package cloudy.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import cloudy.model.entities.InvProducto;
import cloudy.model.entities.InvProductoCaracteristica;
import cloudy.model.entities.InvStock;
import cloudy.model.entities.InvValorEstandar;
import cloudy.model.entities.VenFormaPago;
import cloudy.model.entities.VenRecibo;
import cloudy.model.manager.ManagerDetalle;
import cloudy.model.manager.ManagerEmpresa;
import cloudy.model.manager.ManagerFormaPago;
import cloudy.model.manager.ManagerProductoCaracteristica;
import cloudy.model.manager.ManagerRecibo;
import cloudy.model.manager.ManagerStock;
import cloudy.model.manager.ManagerValorEstandar;

@Named
@SessionScoped
public class BeanCarritoUsuario implements Serializable
{
	private static final long serialVersionUID=1L;
	
	@EJB
	private ManagerProductoCaracteristica managercarrito;
	@EJB
	private ManagerValorEstandar managervalor;
	@EJB
	private ManagerStock managerStock;
	@EJB
	private ManagerDetalle managerDetalle;
	@EJB
	private ManagerRecibo managerRecibo;
	@EJB
	private ManagerEmpresa managerEmpresa;
	@EJB
	private ManagerFormaPago managerformapago;
	
	@Inject
	private BeanUsuario beanusuario;
	
	private List <InvProductoCaracteristica> listaProductos;
	private List <InvProductoCaracteristica> carritoProductos;
	private List <InvValorEstandar> listaValores;
	private List <InvStock> listaStock;
	private List <InvStock> listaStockcarrito;
	private List<VenFormaPago> listadeformasdepago;
	private List<VenRecibo> recibos;
	private List<VenRecibo> recibosporcliente;
	
	
	private InvProductoCaracteristica productoCarrito;
	private double valorProducto;
	private InvProducto p;
	private int idproducto;
	private double totalcarrito=0;
	
	private String nombreFormapago;
	private VenRecibo nuevorecibo;
	
	@PostConstruct
	public void inicializar() 
	{	
		nuevorecibo = new VenRecibo();
		
		listaProductos=managercarrito.findAllProductos();
		listaValores=managervalor.findAllValor();
		listaStock=managerStock.findAllStock();
		listadeformasdepago = managerformapago.FindAllFormaPago();
		recibos = managerRecibo.findAllRecibos();
		recibosporcliente = managerRecibo.findAllRecibosByCliente(beanusuario.getListaPerfilUsuarios().get(0));
	}
	
	
	public void actionListenerAgregarCarrito(InvStock p) 
	{
		listaStockcarrito=managerStock.agregarProductoCarrito(listaStockcarrito, p);
		setTotalcarrito(managervalor.SumaCarrito(listaValores, p.getInvProducto().getIdproducto()));
		System.out.println("bean:"+totalcarrito);
		
	}
	
	public void actionListenerEliminarCarrito(InvStock p) 
	{
		listaStockcarrito=managerStock.eliminarProductoCarrito(listaStockcarrito, p.getInvProducto().getIdproducto());
		setTotalcarrito(managervalor.RestaCarrito(listaValores, p.getInvProducto().getIdproducto()));
		
	}
	
	public double ValorProducto(InvProducto p) 
	{
		
		return valorProducto=managervalor.findValorEstandar(listaValores, p.getIdproducto());
	}
	
	///////METODOS-MILOS////////////
	
	public void actionIngresarRecibo()
	{
		managerRecibo.CrearRecibo(managerEmpresa.FindEmpresaByID(1), beanusuario.getListaPerfilUsuarios().get(0), managerformapago.FindFormaPagoByNombre(nombreFormapago), nuevorecibo);
		recibos = managerRecibo.findAllRecibos();
		nuevorecibo=new VenRecibo();
	}
	
	public void Comprar(List<InvStock> listacarritoStock, VenRecibo recibo)
	{
		int cantidad = 0;
		
		for (int i = 0; i < listacarritoStock.size(); i++)
		{
			InvProducto producto = listacarritoStock.get(0).getInvProducto();
			
			for (int j = 0; j < listacarritoStock.size(); j++)
			{
				if(producto.getCodigoProducto().equals(listacarritoStock.get(j).getInvProducto().getCodigoProducto()))
				{
					cantidad++;
				}
			}
			
			BigDecimal valorProducto = BigDecimal.valueOf(this.ValorProducto(producto));
			managerDetalle.IngresarDetalle(recibo, producto, BigDecimal.valueOf(cantidad), valorProducto, producto.getVenIva());
			cantidad = 0;
			
			for (int j = 0; j < listacarritoStock.size(); j++)
			{
				if(producto.getCodigoProducto().equals(listacarritoStock.get(j).getInvProducto().getCodigoProducto()))
				{
					listacarritoStock.remove(j);
				}
			}
			
			i=0;
		}
	}
	
	public void ComprarYFacturar()
	{
		managerRecibo.CrearRecibo(managerEmpresa.FindEmpresaByID(1), beanusuario.getListaPerfilUsuarios().get(0), managerformapago.FindFormaPagoByNombre(nombreFormapago), nuevorecibo);
		nuevorecibo=new VenRecibo();
		
		recibos = managerRecibo.findAllRecibos();

		VenRecibo recibo = recibos.get(recibos.size()-1);
		
		this.Comprar(listaStockcarrito, recibo);	
		
		managerRecibo.ActualizarValoresRecibo(recibo, BigDecimal.valueOf(managerRecibo.calcularBaseImponible(managerDetalle.FindAllDetallesByRecibo(recibo))), 
													  BigDecimal.valueOf(managerRecibo.calcularBaseCero(managerDetalle.FindAllDetallesByRecibo(recibo))), BigDecimal.valueOf(totalcarrito));
		
		recibosporcliente = managerRecibo.findAllRecibosByCliente(beanusuario.getListaPerfilUsuarios().get(0));
	}
	
	
	//------------------------- getters y setters ---------------------------------------------

	public List<InvProductoCaracteristica> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<InvProductoCaracteristica> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public List<InvProductoCaracteristica> getCarritoProductos() {
		return carritoProductos;
	}

	public void setCarritoProductos(List<InvProductoCaracteristica> carritoProductos) {
		this.carritoProductos = carritoProductos;
	}

	public InvProductoCaracteristica getProductoCarrito() {
		return productoCarrito;
	}

	public void setProductoCarrito(InvProductoCaracteristica productoCarrito) {
		this.productoCarrito = productoCarrito;
	}


	public List<InvValorEstandar> getListaValores() {
		return listaValores;
	}

	public void setListaValores(List<InvValorEstandar> listaValores) {
		this.listaValores = listaValores;
	}

	public InvProducto getP() {
		return p;
	}

	public void setP(InvProducto p) {
		this.p = p;
	}

	public int getIdproducto() {
		return idproducto;
	}

	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}


	public double getValorProducto() {
		return valorProducto;
	}


	public void setValorProducto(double valorProducto) {
		this.valorProducto = valorProducto;
	}


	public List<InvStock> getListaStock() {
		return listaStock;
	}

	public void setListaStock(List<InvStock> listaStock) {
		this.listaStock = listaStock;
	}


	public List<InvStock> getListaStockcarrito() {
		return listaStockcarrito;
	}


	public void setListaStockcarrito(List<InvStock> listaStockcarrito) {
		this.listaStockcarrito = listaStockcarrito;
	}


	public double getTotalcarrito() {
		return totalcarrito;
	}


	public void setTotalcarrito(double totalcarrito) {
		this.totalcarrito = totalcarrito;
	}


	public BeanUsuario getBeanusuario() {
		return beanusuario;
	}


	public void setBeanusuario(BeanUsuario beanusuario) {
		this.beanusuario = beanusuario;
	}


	public List<VenFormaPago> getListadeformasdepago() {
		return listadeformasdepago;
	}


	public void setListadeformasdepago(List<VenFormaPago> listadeformasdepago) {
		this.listadeformasdepago = listadeformasdepago;
	}


	public String getNombreFormapago() {
		return nombreFormapago;
	}


	public void setNombreFormapago(String nombreFormapago) {
		this.nombreFormapago = nombreFormapago;
	}


	public VenRecibo getNuevorecibo() {
		return nuevorecibo;
	}


	public void setNuevorecibo(VenRecibo nuevorecibo) {
		this.nuevorecibo = nuevorecibo;
	}


	public List<VenRecibo> getRecibos() {
		return recibos;
	}


	public void setRecibos(List<VenRecibo> recibos) {
		this.recibos = recibos;
	}


	public List<VenRecibo> getRecibosporcliente() {
		return recibosporcliente;
	}


	public void setRecibosporcliente(List<VenRecibo> recibosporcliente) {
		this.recibosporcliente = recibosporcliente;
	}
	
	
}
