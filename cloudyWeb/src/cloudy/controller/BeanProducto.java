package cloudy.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import cloudy.model.entities.InvMarcaProducto;
import cloudy.model.entities.InvProducto;
import cloudy.model.entities.InvTipoCaracteristica;
import cloudy.model.entities.InvTipoProducto;
import cloudy.model.entities.VenFormaPago;
import cloudy.model.entities.VenIva;
import cloudy.model.manager.ManagerFormaPago;
import cloudy.model.manager.ManagerIVA;
import cloudy.model.manager.ManagerMarca;
import cloudy.model.manager.ManagerProducto;
import cloudy.model.manager.ManagerProductoCaracteristica;
import cloudy.model.manager.ManagerTipoCarac;
import cloudy.model.manager.ManagerTipoProducto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Named
@SessionScoped
public class BeanProducto implements Serializable 
{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ManagerProducto managerproducto;
	@EJB
	private ManagerProductoCaracteristica managerproductocaracteristica;
	@EJB
	private ManagerTipoCarac managertipocaracteristica;
	@EJB
	private ManagerMarca managermarca;
	@EJB
	private ManagerIVA manageriva;
	@EJB
	private ManagerTipoProducto managertipoproduct;
	@EJB
	private ManagerFormaPago managerformapago;
	
	//------------------ LISTAS -----------------------
	private List<InvTipoCaracteristica> listadetiposcaracteristicas;
	private List<VenIva> listadeIVAS;
	private List<InvMarcaProducto> listademarcas;
	private List<InvTipoProducto> listadetiposdeproductos;
	
	//---------------- OBJETOS ------------------
	private InvProducto nuevoProducto;
	private VenIva nuevoiva;
	private InvMarcaProducto nuevamarca;
	private InvTipoProducto nuevotipoproducto;
	private VenFormaPago nuevaFormaPago;
	
	private VenIva iva;
	private InvMarcaProducto marca;
	
	private InvTipoCaracteristica tipoMod;
	private InvTipoCaracteristica bateriaMod;
	private InvTipoCaracteristica bottomfeedermod;
	private InvTipoCaracteristica roscamod;
	
	private InvTipoCaracteristica tipoAtomizador;
	private InvTipoCaracteristica tamanoAtomizador;
	private InvTipoCaracteristica bottomfeederAtomizador;
	private InvTipoCaracteristica roscaAtomizador;
	
	private InvTipoCaracteristica tipoResistencia;
	private InvTipoCaracteristica rangomaxResistencia;
	private InvTipoCaracteristica rangominResistencia;
	private InvTipoCaracteristica OhmiosResistencia;
	
	private InvTipoCaracteristica tipoAlgodon;
	
	private InvTipoCaracteristica saborEsencia;
	private InvTipoCaracteristica nictinaEsencia;
	
	
	private InvTipoProducto tipoProducto;
	
	//------------------ IDES -----------------------
	private int idMarca;
	private int idIva;
	private int idtipoproducto;
	
	private String ivatpm;
	private String datoivatpm;
	
	//----------------- DESCRIPCIONES-------------------
	private String descMAR1;
	private String descMAR2;
	private String descMAR3;
	private String descMAR4;
	
	
	@PostConstruct
	public void Inicializar()
	{
		nuevoProducto = new InvProducto();
		iva = new VenIva();
		marca = new InvMarcaProducto();
		tipoProducto = new InvTipoProducto();
		nuevoiva = new VenIva();
		nuevamarca = new InvMarcaProducto();
		nuevotipoproducto = new InvTipoProducto();
		nuevaFormaPago = new VenFormaPago();
		
		listadeIVAS = manageriva.findAllIVA();
		listademarcas = managermarca.findAllMarcas();
		listadetiposdeproductos = managertipoproduct.findAllTiposdeProductos();
	}
	
	public void actionIngresarMod()
	{
		marca = managermarca.findMarca(idMarca);
		iva = manageriva.findIVAbyID(idIva);
		tipoProducto = managertipoproduct.FindTipoProductoByID(1);
		
		tipoMod = managertipocaracteristica.findTipoMod();
		bateriaMod = managertipocaracteristica.findTipoBateriaMod();
		bottomfeedermod = managertipocaracteristica.findBottomFeederMod();
		roscamod = managertipocaracteristica.findTipoRoscaMod();
		
		managerproductocaracteristica.IngresarModORAtomizadorORResistencia(managerproducto.IngresarProducto(iva, marca, nuevoProducto, tipoProducto), tipoMod, descMAR1, bateriaMod, descMAR2, bottomfeedermod, descMAR3, roscamod, descMAR4);
		
		nuevoProducto = new InvProducto();	
		iva = new VenIva();
		marca = new InvMarcaProducto();
		tipoProducto = new InvTipoProducto();
	}
	
	public void actionIngresarAtomizador()
	{
		marca = managermarca.findMarca(idMarca);
		iva = manageriva.findIVAbyID(idIva);
		tipoProducto = managertipoproduct.FindTipoProductoByID(2);
		
		tipoAtomizador = managertipocaracteristica.findTipoAtomizador();
		tamanoAtomizador = managertipocaracteristica.findTamanoAtomizador();
		bottomfeederAtomizador = managertipocaracteristica.findBottomFeederAtomizador();
		roscaAtomizador = managertipocaracteristica.findTipoRoscaAtomizador();
		
		managerproductocaracteristica.IngresarModORAtomizadorORResistencia(managerproducto.IngresarProducto(iva, marca, nuevoProducto, tipoProducto), tipoAtomizador, descMAR1, tamanoAtomizador, descMAR2, bottomfeederAtomizador, descMAR3, roscaAtomizador, descMAR4);
		
		nuevoProducto = new InvProducto();	
		iva = new VenIva();
		marca = new InvMarcaProducto();
		tipoProducto = new InvTipoProducto();
		
	}
	
	public void actionIngresarResistencia()
	{
		marca = managermarca.findMarca(idMarca);
		iva = manageriva.findIVAbyID(idIva);
		tipoProducto = managertipoproduct.FindTipoProductoByID(3);
		
		tipoResistencia = managertipocaracteristica.findTipoResistencia();
		rangomaxResistencia = managertipocaracteristica.findTipoRangoMaximoResistencia();
		rangominResistencia = managertipocaracteristica.findTipoRangoMinimoResistencia();
		OhmiosResistencia = managertipocaracteristica.findTipoOhmiosResistencia();
		
		managerproductocaracteristica.IngresarModORAtomizadorORResistencia(managerproducto.IngresarProducto(iva, marca, nuevoProducto, tipoProducto), tipoResistencia, descMAR1, rangomaxResistencia, descMAR2, rangominResistencia, descMAR3, OhmiosResistencia, descMAR4);
		
		nuevoProducto = new InvProducto();	
		iva = new VenIva();
		marca = new InvMarcaProducto();
		tipoProducto = new InvTipoProducto();
	}
	
	public void actionIngresarAlgodon()
	{
		marca = managermarca.findMarca(idMarca);
		iva = manageriva.findIVAbyID(idIva);
		tipoProducto = managertipoproduct.FindTipoProductoByID(4);
		
		tipoAlgodon = managertipocaracteristica.findTipoAlgodon();
		
		managerproductocaracteristica.IngresarAlgodon(managerproducto.IngresarProducto(iva, marca, nuevoProducto, tipoProducto), tipoAlgodon, descMAR1);
		
		nuevoProducto = new InvProducto();	
		iva = new VenIva();
		marca = new InvMarcaProducto();
		tipoProducto = new InvTipoProducto();
	}
	
	public void actionIngresarEsencia()
	{
		marca = managermarca.findMarca(idMarca);
		iva = manageriva.findIVAbyID(idIva);
		tipoProducto = managertipoproduct.FindTipoProductoByID(7);
		
		saborEsencia = managertipocaracteristica.findTipoSaborEsencia();
		nictinaEsencia = managertipocaracteristica.findTipoNicotinaEsencia();
		
		managerproductocaracteristica.IngresarEsencia(managerproducto.IngresarProducto(iva, marca, nuevoProducto, tipoProducto), saborEsencia, descMAR1, nictinaEsencia, descMAR2);
		
		nuevoProducto = new InvProducto();	
		iva = new VenIva();
		marca = new InvMarcaProducto();
		tipoProducto = new InvTipoProducto();
	}
	
	public void actionIgresarProduuctosVarios()
	{
		marca = managermarca.findMarca(idMarca);
		iva = manageriva.findIVAbyID(idIva);
		tipoProducto = managertipoproduct.FindTipoProductoByID(idtipoproducto);
		
		managerproducto.IngresarProducto(iva, marca, nuevoProducto, tipoProducto);
		
		nuevoProducto = new InvProducto();
		iva = new VenIva();
		marca = new InvMarcaProducto();
		tipoProducto = new InvTipoProducto();
	}
	
	public void actionIngresarIVA_TP_M()
	{
		if(ivatpm.equals("I.V.A"))
		{
			nuevoiva.setIva(BigDecimal.valueOf(Double.parseDouble(datoivatpm)));
			manageriva.IngresarIva(nuevoiva);
			listadeIVAS = manageriva.findAllIVA();
			nuevoiva = new VenIva();
		}else
			if(ivatpm.equals("Tipo de Producto"))
			{
				nuevotipoproducto.setNomtipoproducto(datoivatpm);
				managertipoproduct.INgresarNuevoTipoDeProducto(nuevotipoproducto);
				listadetiposdeproductos = managertipoproduct.findAllTiposdeProductos();
				nuevotipoproducto = new InvTipoProducto();
			}else
				if(ivatpm.equals("Marcas"))
				{
					nuevamarca.setNombreMarca(datoivatpm);
					managermarca.IngresarNuevaMarca(nuevamarca);
					listademarcas = managermarca.findAllMarcas();
					nuevamarca = new InvMarcaProducto();
				}else
					if(ivatpm.equals("Forma de Pago"))
					{
						nuevaFormaPago.setNombreFormaPago(datoivatpm);
						managerformapago.IngresarFormaDePago(nuevaFormaPago);
						nuevaFormaPago = new VenFormaPago();
					}
	}
	
	//-------------------- geters and setters -------------------

	public List<InvTipoCaracteristica> getListadetiposcaracteristicas() {
		return listadetiposcaracteristicas;
	}


	public void setListadetiposcaracteristicas(List<InvTipoCaracteristica> listadetiposcaracteristicas) {
		this.listadetiposcaracteristicas = listadetiposcaracteristicas;
	}


	public List<VenIva> getListadeIVAS() {
		return listadeIVAS;
	}


	public void setListadeIVAS(List<VenIva> listadeIVAS) {
		this.listadeIVAS = listadeIVAS;
	}


	public List<InvMarcaProducto> getListademarcas() {
		return listademarcas;
	}


	public void setListademarcas(List<InvMarcaProducto> listademarcas) {
		this.listademarcas = listademarcas;
	}


	public int getIdMarca() {
		return idMarca;
	}


	public void setIdMarca(int idMarca) {
		this.idMarca = idMarca;
	}


	public int getIdIva() {
		return idIva;
	}


	public void setIdIva(int idIva) {
		this.idIva = idIva;
	}

	public InvProducto getNuevoProducto() {
		return nuevoProducto;
	}

	public void setNuevoProducto(InvProducto nuevoProducto) {
		this.nuevoProducto = nuevoProducto;
	}

	public VenIva getIva() {
		return iva;
	}

	public void setIva(VenIva iva) {
		this.iva = iva;
	}

	public InvMarcaProducto getMarca() {
		return marca;
	}

	public void setMarca(InvMarcaProducto marca) {
		this.marca = marca;
	}

	public List<InvTipoProducto> getListadetiposdeproductos() {
		return listadetiposdeproductos;
	}

	public void setListadetiposdeproductos(List<InvTipoProducto> listadetiposdeproductos) {
		this.listadetiposdeproductos = listadetiposdeproductos;
	}

	public InvTipoProducto getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(InvTipoProducto tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public int getIdtipoproducto() {
		return idtipoproducto;
	}

	public void setIdtipoproducto(int idtipoproducto) {
		this.idtipoproducto = idtipoproducto;
	}

	public String getDescMAR1() {
		return descMAR1;
	}

	public void setDescMAR1(String descMAR1) {
		this.descMAR1 = descMAR1;
	}

	public String getDescMAR2() {
		return descMAR2;
	}

	public void setDescMAR2(String descMAR2) {
		this.descMAR2 = descMAR2;
	}

	public String getDescMAR3() {
		return descMAR3;
	}

	public void setDescMAR3(String descMAR3) {
		this.descMAR3 = descMAR3;
	}

	public String getDescMAR4() {
		return descMAR4;
	}

	public void setDescMAR4(String descMAR4) {
		this.descMAR4 = descMAR4;
	}

	public VenIva getNuevoiva() {
		return nuevoiva;
	}

	public void setNuevoiva(VenIva nuevoiva) {
		this.nuevoiva = nuevoiva;
	}

	public InvMarcaProducto getNuevamarca() {
		return nuevamarca;
	}

	public void setNuevamarca(InvMarcaProducto nuevamarca) {
		this.nuevamarca = nuevamarca;
	}

	public InvTipoProducto getNuevotipoproducto() {
		return nuevotipoproducto;
	}

	public void setNuevotipoproducto(InvTipoProducto nuevotipoproducto) {
		this.nuevotipoproducto = nuevotipoproducto;
	}

	public String getIvatpm() {
		return ivatpm;
	}

	public void setIvatpm(String ivatpm) {
		this.ivatpm = ivatpm;
	}

	public String getDatoivatpm() {
		return datoivatpm;
	}

	public void setDatoivatpm(String datoivatpm) {
		this.datoivatpm = datoivatpm;
	}

	public VenFormaPago getNuevaFormaPago() {
		return nuevaFormaPago;
	}

	public void setNuevaFormaPago(VenFormaPago nuevaFormaPago) {
		this.nuevaFormaPago = nuevaFormaPago;
	}
}
