package cloudy.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import cloudy.model.dto.LoginDTO;
import cloudy.model.entities.Bitacora;
import cloudy.model.entities.Dpa;
import cloudy.model.entities.UsuaRole;
import cloudy.model.entities.UsuaUsuario;
import cloudy.model.manager.ManagerAuditoria;
import cloudy.model.manager.ManagerDPA;
import cloudy.model.manager.ManagerRoles;
import cloudy.model.manager.ManagerSeguridad;
import cloudy.model.manager.ManagerUsuario;
import cloudy.model.util.ModelUtil;
import jdk.nashorn.internal.runtime.JSONFunctions;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named
@SessionScoped
public class BeanUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerUsuario managerusuario;
	@EJB
	private ManagerRoles managerrol;
	@EJB
	private ManagerDPA managerdpa;
	@EJB
	private ManagerSeguridad managerSeguridad;
	@EJB
	private ManagerAuditoria managerAuditoria;

	// --------------- LISTAS -----------------------------//
	private List<UsuaUsuario> listaUsuarios;
	private List<UsuaUsuario> listaPerfilUsuarios;
	private List<UsuaUsuario> listaDeClientes;
	private List<UsuaUsuario> listaDeVendedores;
	private List<UsuaRole> listaRoles;
	private List<Bitacora> listaBitacora;

	private List<Dpa> listaprovincias;
	private List<Dpa> listaCantones;
	private List<Dpa> listaCantonesporProvicnia;

	// --------------- OBJETOS ------------------------//
	private UsuaUsuario nuevousuario;
	private UsuaUsuario usuarioSeleccionado;
	private Dpa lugar;

	private int idUsuario;

	private String idProvincia;
	private String idCanton;
	private int idCliente;

	private UsuaRole usuarioVendedor;
	private UsuaRole usuarioAdministrador;
	private UsuaRole usuarioCliente;

	private LoginDTO loginDTO;

	private String nombre;
	private String apellido;
	private String email;
	private String telefono;
	private String contrasena;

	@PostConstruct
	public void Inicializar() {
		loginDTO = new LoginDTO();

		nuevousuario = new UsuaUsuario();
		lugar = new Dpa();

		listaprovincias = managerdpa.findAllProvincias();
		listaCantones = managerdpa.findAllCantones();
		listaUsuarios = managerusuario.findAllUsuario();
		listaDeClientes = managerusuario.AllClientes();
		listaDeVendedores = managerusuario.AllVendedores();
		listaRoles = managerrol.findAllRoles();
		listaBitacora = managerAuditoria.findAllBitacoras();

		usuarioVendedor = managerrol.FindVendedor();
		usuarioAdministrador = managerrol.FindAdministrador();
		usuarioCliente = managerrol.FindUsuario();
	}

	public void actionListadeCantones() {
		listaCantonesporProvicnia = managerdpa.findCantonesPorProvincia(listaCantones, idProvincia);
	}

	public void actionIngresarVendedor() {
		lugar = managerdpa.findDPA(idCanton);
		managerusuario.NewVendedor(nuevousuario, usuarioVendedor, lugar);

		listaUsuarios = managerusuario.findAllUsuario();
		listaDeVendedores = managerusuario.AllVendedores();
		nuevousuario = new UsuaUsuario();
		lugar = new Dpa();
	}

	public String actionIngresarCliente() {
		lugar = managerdpa.findDPA(idCanton);
		managerusuario.NewVendedor(nuevousuario, usuarioCliente, lugar);

		listaUsuarios = managerusuario.findAllUsuario();
		listaDeClientes = managerusuario.AllClientes();
		nuevousuario = new UsuaUsuario();
		lugar = new Dpa();

		return "login.xhtml";
	}

	/**
	 * Action que permite el acceso al sistema.
	 * 
	 * @return
	 */
	public String accederSistema() {
		try {
			loginDTO = managerSeguridad.accederSistema(email, contrasena);
			idCliente = loginDTO.getIdusuario();
			listaPerfilUsuarios = managerusuario.findPerfilByIdUsuario(idCliente);
			// redireccion dependiendo del tipo de usuario:
			return loginDTO.getRutaAcceso() + "?faces-redirect=true";
		} catch (Exception e) {
			e.printStackTrace();

			JSFUtil.crearMensajeERROR(e.getMessage());
		}
		return "";
	}

	/**
	 * Finaliza la sesion web del usuario.
	 * 
	 * @return
	 */
	public String salirSistema() {
		System.out.println("salirSistema");
		try {
			managerAuditoria.crearEvento(loginDTO.getIdusuario(), this.getClass(), "salirSistema", "Cerrar sesion");
		} catch (Exception e) {
			e.printStackTrace();
		}
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/landingPage.xhtml?faces-redirect=true";
	}

	public void actionVerificarLogin() {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		String requestPath = ec.getRequestPathInfo();
		try {
			// si no paso por login:
			if (loginDTO == null || ModelUtil.isEmpty(loginDTO.getRutaAcceso())) {
				ec.redirect(ec.getRequestContextPath() + "/index.html");
			} else {
				// validar las rutas de acceso:
				if (requestPath.contains("/administrador") && loginDTO.getRutaAcceso().startsWith("/administrador"))
					return;
				if (requestPath.contains("/vendedor") && loginDTO.getRutaAcceso().startsWith("/vendedor"))
					return;
				if (requestPath.contains("/cliente") && loginDTO.getRutaAcceso().startsWith("/cliente"))
					return;
				// caso contrario significa que hizo login pero intenta acceder a ruta no
				// permitida:
				ec.redirect(ec.getRequestContextPath() + "/landingPage.html");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// -------------------------------Login Funcional-------------------------------
	/*
	 * public String actionLogin() { listaUsuarios =
	 * managerusuario.findAllUsuario(); for(UsuaUsuario u: listaUsuarios) {
	 * if(u.getEmail().equals(nuevousuario.getEmail()) &&
	 * u.getContrasena().equals(nuevousuario.getContrasena())) {
	 * nuevousuario.setUsuaRole(u.getUsuaRole()); idCliente=u.getIdusuario(); break;
	 * } } System.out.println("//////////////"+idCliente); listaPerfilUsuarios =
	 * managerusuario.findPerfilByIdUsuario(idCliente);
	 * 
	 * if(nuevousuario.getUsuaRole().getIdrol().equals(3)) { nuevousuario = new
	 * UsuaUsuario(); return "/cliente/PerfilCliente.xhtml"; }else {
	 * if(nuevousuario.getUsuaRole().getIdrol().equals(2)) { nuevousuario = new
	 * UsuaUsuario(); return "/vendedor/Vendedor-Formulario-Venta.xhtml"; }else {
	 * if(nuevousuario.getUsuaRole().getIdrol().equals(1)) { nuevousuario = new
	 * UsuaUsuario(); return "/administrador/Registro-Administrador.xhtml"; } } }
	 * 
	 * 
	 * 
	 * return "loginCliente"; }
	 */

	// Perfil Cliente////////////////
	public void actionListenerSeleccionarPerfil() {
		usuarioSeleccionado = managerusuario.findByIdUsuario(idCliente);
		System.out.println("////////el usuario es://////" + usuarioSeleccionado.getNombre());
		
	}

	public String actionActualizarPerfil() {
		System.out.println("////////el id es://////" + idCliente);
	

		managerusuario.actualizarUsuario(usuarioSeleccionado);
		listaUsuarios = managerusuario.findAllUsuario();
		listaPerfilUsuarios = managerusuario.findPerfilByIdUsuario(idCliente);
		return "PerfilCliente";
	}

	public String actionRegresarPerfil() {
		nuevousuario = new UsuaUsuario();
		return "PerfilCliente";
	}

	public void actionListenerConsultarPerfil() {
		listaUsuarios = managerusuario.findPerfilByIdUsuario(idCliente);
	}

	// -----------------------Reportes--------------------------------
	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		/*
		 * parametros.put("p_titulo_principal",p_titulo_principal);
		 * parametros.put("p_titulo",p_titulo);
		 */
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("administrador/cloudy.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=bitacora.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cloudydb", "postgres", "1459");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	// --------------------Redirecciones-------------------------------
	public String actionRedirigirLogin() {
		return "login.xhtml";
	}

	public String actionRedirigirRegistro() {
		return "registroCliente.xhtml";
	}

	// ------------------- gets y sets -------------------------------------------

	public List<UsuaUsuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<UsuaUsuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<Dpa> getListaprovincias() {
		return listaprovincias;
	}

	public void setListaprovincias(List<Dpa> listaprovincias) {
		this.listaprovincias = listaprovincias;
	}

	public List<Dpa> getListaCantones() {
		return listaCantones;
	}

	public void setListaCantones(List<Dpa> listaCantones) {
		this.listaCantones = listaCantones;
	}

	public UsuaUsuario getNuevousuario() {
		return nuevousuario;
	}

	public void setNuevousuario(UsuaUsuario nuevousuario) {
		this.nuevousuario = nuevousuario;
	}

	public Dpa getLugar() {
		return lugar;
	}

	public void setLugar(Dpa lugar) {
		this.lugar = lugar;
	}

	public UsuaRole getUsuarioVendedor() {
		return usuarioVendedor;
	}

	public void setUsuarioVendedor(UsuaRole usuarioVendedor) {
		this.usuarioVendedor = usuarioVendedor;
	}

	public UsuaRole getUsuarioAdministrador() {
		return usuarioAdministrador;
	}

	public void setUsuarioAdministrador(UsuaRole usuarioAdministrador) {
		this.usuarioAdministrador = usuarioAdministrador;
	}

	public UsuaRole getUsuarioCliente() {
		return usuarioCliente;
	}

	public void setUsuarioCliente(UsuaRole usuarioCliente) {
		this.usuarioCliente = usuarioCliente;
	}

	public String getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(String idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getIdCanton() {
		return idCanton;
	}

	public void setIdCanton(String idCanton) {
		this.idCanton = idCanton;
	}

	public List<Dpa> getListaCantonesporProvicnia() {
		return listaCantonesporProvicnia;
	}

	public void setListaCantonesporProvicnia(List<Dpa> listaCantonesporProvicnia) {
		this.listaCantonesporProvicnia = listaCantonesporProvicnia;
	}

	public List<UsuaUsuario> getListaPerfilUsuarios() {
		return listaPerfilUsuarios;
	}

	public void setListaPerfilUsuarios(List<UsuaUsuario> listaPerfilUsuarios) {
		this.listaPerfilUsuarios = listaPerfilUsuarios;
	}

	public List<UsuaRole> getListaRoles() {
		return listaRoles;
	}

	public void setListaRoles(List<UsuaRole> listaRoles) {
		this.listaRoles = listaRoles;
	}

	public int getIdUsuario() {
		return idCliente;
	}

	public void setIdUsuario(int idUsuario) {
		this.idCliente = idUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public LoginDTO getLoginDTO() {
		return loginDTO;
	}

	public void setLoginDTO(LoginDTO loginDTO) {
		this.loginDTO = loginDTO;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<UsuaUsuario> getListaDeClientes() {
		return listaDeClientes;
	}

	public void setListaDeClientes(List<UsuaUsuario> listaDeClientes) {
		this.listaDeClientes = listaDeClientes;
	}

	public List<UsuaUsuario> getListaDeVendedores() {
		return listaDeVendedores;
	}

	public void setListaDeVendedores(List<UsuaUsuario> listaDeVendedores) {
		this.listaDeVendedores = listaDeVendedores;
	}

	public List<Bitacora> getListaBitacora() {
		return listaBitacora;
	}

	public void setListaBitacora(List<Bitacora> listaBitacora) {
		this.listaBitacora = listaBitacora;
	}

	public UsuaUsuario getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}

	public void setUsuarioSeleccionado(UsuaUsuario usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}
	
	
	
}
